<?php

return [
    'user' => [
        'status' => [
            1 => 'Pending',
            2 => 'Active',
            3 => 'Inactive',
            4 => 'Banned'
        ]
    ],
    'bank' => [
        'type' => [
            1 => 'Bank Umum Persero',
            2 => 'Bank Umum Swasta Nasional',
            3 => 'Bank Pembangunan Daerah',
            4 => 'Bank Asing'
        ]
    ],
    'card' => [
        'type' => [
            1 => 'Credit Card',
            2 => 'Debit Card'
        ]
    ]
];