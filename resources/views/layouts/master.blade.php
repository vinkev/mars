<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--Title and Meta--}}
    @meta

    @yield('styles_before')

    {{ Html::style(mix('assets/app/css/app.css')) }}

    @yield('styles_after')

    {{--Head--}}
    @yield('head')

    {{--Common Scripts--}}
    {{ Html::script(mix('assets/app/js/app.js')) }}

    @yield('header_scripts')

</head>
<body class="@yield('body_class')">

{{--Page--}}
@yield('page')


{{--Laravel Js Variables--}}
{{--@tojs--}}

{{--Scripts--}}
@yield('footer_scripts')
@if (app()->environment('local'))
    <script id="__bs_script__">//<![CDATA[
        document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.6'><\/script>".replace("HOST", location.hostname));
        //]]></script>
@endif
</body>
</html>