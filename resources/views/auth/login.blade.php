@extends('auth.layouts.master_auth')

@section('body_class','login')

@section('content')
    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                {{ Form::open(['action' => 'Auth\LoginController@login']) }}
                <h1>Login Form</h1>

                <div>
                    {{ Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'E-Mail Address', 'required', 'autofocus']) }}
                </div>
                <div>
                    {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'required']) }}
                </div>
                <div class="checkbox text-left">
                    <label>
                        {{ Form::checkbox('remember') }} Remember me
                    </label>
                </div>

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if (!$errors->isEmpty())
                    <div class="alert alert-danger" role="alert">
                        {!! $errors->first() !!}
                    </div>
                @endif

                <div>
                    <button class="btn btn-default submit" type="submit">Log In</button>
                    <a class="reset_pass" href="{{ action('Auth\ForgotPasswordController@showLinkRequestForm') }}">
                        Lost your password?
                    </a>
                </div>

                {{ Form::close() }}
            </section>
        </div>
    </div>
@endsection

@section('styles_before')
    @parent

    {{ Html::style(mix('assets/auth/css/login.css')) }}
@endsection