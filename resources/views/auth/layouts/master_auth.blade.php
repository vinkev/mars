@extends('layouts.master')

@section('page')

    {{--Region Content--}}
    @yield('content')

@endsection

@section('styles_before')
    {{ Html::style(mix('assets/auth/css/auth.css')) }}
@show