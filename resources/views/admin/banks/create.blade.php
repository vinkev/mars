@extends('admin.layouts.master_admin')

@section('title', 'Create Bank')

@section('content')
    @include('admin.banks.form',[
      'form_url' => action('Admin\BankController@store'),
      'method' => 'post',
      'button_text' => 'Create Bank'
    ])
@endsection