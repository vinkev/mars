@extends('admin.layouts.master_admin')

@section('title', 'Banks List')

@section('title-right')
    <div class="pull-right">
        <a href="{!! action('Admin\BankController@create') !!}" class="{!! $button['create'] !!} w-md"><i class="fa fa-plus-square" aria-hidden="true"></i> Create New Bank</a>
    </div>
@endsection

@section('content')
    <div class="x_panel">
        <div class="x_content">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Nickname</th>
                    <th class="text-center">Logo</th>
                    <th class="text-center">Enable</th>
                    <th class="col-sm-1 text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($banks as $key => $bank)
                    <tr>
                        <td>{!! (($banks->currentPage() - 1) * $banks->perPage()) + $key + 1 !!}</td>
                        <td>{!! $bank->bank_name !!}</td>
                        <td>{!! $bank->bank_nickname !!}</td>
                        <td class="text-center"><img class="img-thumbnail" src="{!! ImageHelper::tinyHeighten($bank->logo) !!}"></td>
                        <td class="text-center">
                            @if($bank->is_enabled)
                                <i class="fa fa-check-circle fa-lg text-success" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-times-circle fa-lg text-danger" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td class="nowrap">
                            <a href="{!! action('Admin\BankController@edit', $bank->uuid) !!}" class="{!! $button['edit'] !!} btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                            {!! Form::open(['class' => 'form-delete', 'url' => action('Admin\BankController@destroy', $bank->uuid), 'method' => 'DELETE']) !!}
                            {!! Form::hidden('_method', 'delete') !!}
                            <button type="submit" class="{!! $button['delete'] !!} btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $banks->links() !!}
            </div>
        </div>
    </div>
@endsection
