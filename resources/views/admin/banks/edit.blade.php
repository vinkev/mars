@extends('admin.layouts.master_admin')

@section('title', 'Edit Bank')

@section('content')
    @include('admin.banks.form',[
      'form_url' => action('Admin\BankController@update', $bank->uuid),
      'method' => 'put',
      'button_text' => 'Update Bank'
    ])
@endsection

