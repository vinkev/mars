<div class="row">
    <div class="col-sm-12">
        <div class="x_panel">
            <div class="x_content">
                {!! Form::open(['class' => 'form-horizontal', 'url' => $form_url, 'method' => $method, 'files' => true]) !!}
                <div class="form-group">
                    <label class="control-label col-sm-3" for="is_enabled">Is Enabled</label>
                    <div class="col-sm-6">
                        <div class="checkbox">
                            {!! Form::checkbox('is_enabled', 1, $bank->is_enabled, ['class' => 'switchery form-control', 'checked' => 'checked']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="bank_name">Bank Name</label>
                    <div class="col-sm-6">
                        {!! Form::text('bank_name', $bank->bank_name, ['class' => 'form-control' . ($errors->has('bank_name') ? ' parsley-error' : '') , 'placeholder' => 'Bank Name', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'bank_name'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="bank_nickname">Bank Nickname</label>
                    <div class="col-sm-6">
                        {!! Form::text('bank_nickname', $bank->bank_nickname, ['class' => 'form-control' . ($errors->has('bank_nickname') ? ' parsley-error' : '') , 'placeholder' => 'Bank Nickname', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'bank_nickname'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="bank_type">Bank Type</label>
                    <div class="col-sm-6">
                        {!! Form::select('bank_type', $bank_type, $bank->bank_type, ['class' => 'form-control' . ($errors->has('bank_type') ? ' parsley-error' : '') , 'data-placeholder' => 'Choose Bank Type', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'bank_type'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="transfer_bank_code">Transfer Bank Code</label>
                    <div class="col-sm-3">
                        {!! Form::text('transfer_bank_code', $bank->transfer_bank_code, ['class' => 'form-control' . ($errors->has('transfer_bank_code') ? ' parsley-error' : '') , 'placeholder' => 'Transfer Bank Code', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="col-sm-offset-3 col-sm-6">
                        @include('admin.layouts.form_error', ['field_name' => 'transfer_bank_code'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="logo">Logo</label>
                    <div class="col-sm-6">
                        {!! Form::file('logo', ['class' => 'file-upload' . ($errors->has('logo') ? ' parsley-error' : ''), 'data-max-file-size' => config('domain.general.max_upload.bank.logo'), 'data-allowed-file-types' => '["image"]', 'data-initial-preview' => !empty($bank->logo) ? '["' . asset('storage/' . $bank->logo) . '"]' : '']) !!}
                        {!! Form::hidden('logo_path', $bank->logo, ['class' => 'file-upload-path']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'logo'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="website">Website</label>
                    <div class="col-sm-6">
                        {!! Form::text('website', $bank->website, ['class' => 'form-control' . ($errors->has('website') ? ' parsley-error' : '') , 'placeholder' => 'Website', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'website'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="description">Description</label>
                    <div class="col-sm-6">
                        {!! Form::textarea('description', $bank->description, ['class' => 'form-control' . ($errors->has('description') ? ' parsley-error' : ''), 'placeholder' => 'Description']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'description'])
                    </div>
                </div>
                <div class="ln_solid"></div>

                <div class="form-group">
                    <label class="control-label col-sm-3" for="location_name">Location Name / Building Name</label>
                    <div class="col-sm-6">
                        {!! Form::text('location_name', $bank->address->location_name, ['class' => 'form-control' . ($errors->has('location_name') ? ' parsley-error' : '') , 'placeholder' => 'Location Name', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'location_name'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="location_info">Location Info / Floor Info</label>
                    <div class="col-sm-6">
                        {!! Form::text('location_info', $bank->address->location_info, ['class' => 'form-control' . ($errors->has('location_info') ? ' parsley-error' : '') , 'placeholder' => 'Location Info', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'location_info'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="address">Address</label>
                    <div class="col-sm-6">
                        {!! Form::text('address', $bank->address->address, ['class' => 'form-control' . ($errors->has('address') ? ' parsley-error' : '') , 'placeholder' => 'Address', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'address'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="city">City</label>
                    <div class="col-sm-6">
                        {!! Form::text('city', $bank->address->city, ['class' => 'form-control' . ($errors->has('city') ? ' parsley-error' : '') , 'placeholder' => 'City', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'city'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="state">State</label>
                    <div class="col-sm-6">
                        {!! Form::select('state', $states, $bank->address->state, ['class' => 'form-control' . ($errors->has('state') ? ' parsley-error' : '') , 'data-placeholder' => 'Choose State', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'state'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="zip_code">Zip Code</label>
                    <div class="col-sm-2">
                        {!! Form::text('zip_code', $bank->address->zip_code, ['class' => 'form-control' . ($errors->has('zip_code') ? ' parsley-error' : '') , 'placeholder' => 'Zip Code', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-offset-3 col-sm-9">
                        @include('admin.layouts.form_error', ['field_name' => 'zip_code'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="office_phone">Office Phone</label>
                    @for($i = 0; $i <= 3; $i++)
                        <div class="col-sm-3 {!! $i > 0 ? 'col-sm-offset-3' : '' !!}">
                            {!! Form::text('office_phone[]', !empty($bank->address->office_phone[$i]) ? $bank->address->office_phone[$i] : '' , ['class' => 'form-control' . ($errors->has('office_phone.' . $i) ? ' parsley-error' : '') , 'placeholder' => 'Office Phone', 'autocomplete' => 'off']) !!}
                        </div>
                        <div class="col-sm-6">
                            @include('admin.layouts.form_error', ['field_name' => 'office_phone.' . $i])
                        </div>
                        <div class="clearfix"></div>
                    @endfor
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="fax_phone">Fax Phone</label>
                    @for($i = 0; $i <=3; $i++)
                        <div class="col-sm-3 {!! $i > 0 ? 'col-sm-offset-3' : '' !!}">
                            {!! Form::text('fax_phone[]', !empty($bank->address->fax_phone[$i]) ? $bank->address->fax_phone[$i] : '', ['class' => 'form-control' . ($errors->has('fax_phone.' . $i) ? ' parsley-error' : '') , 'placeholder' => 'Fax Phone', 'autocomplete' => 'off']) !!}
                        </div>
                        <div class="col-sm-6">
                            @include('admin.layouts.form_error', ['field_name' => 'fax_phone.' . $i])
                        </div>
                        <div class="clearfix"></div>
                    @endfor
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="{!! $button['submit'] !!} w-md" name="submit">{!! $button_text !!}</button>
                        <a href="{!! action('Admin\BankController@index') !!}" class="{!! $button['cancel'] !!} w-md">Cancel</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('styles_before')
    @parent

    {!! Html::style(mix('assets/admin/css/edit.css')) !!}
@endsection

@section('footer_scripts')
    @parent

    {!! Html::script(mix('assets/admin/js/edit.js')) !!}
@endsection
