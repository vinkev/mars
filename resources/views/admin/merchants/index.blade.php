@extends('admin.layouts.master_admin')

@section('title', 'Merchants List')

@section('title-right')
    <div class="pull-right">
        <a href="{!! action('Admin\MerchantController@create') !!}" class="{!! $button['create'] !!} w-md"><i class="fa fa-plus-square" aria-hidden="true"></i> Create New Merchant</a>
    </div>
@endsection

@section('content')
    <div class="x_panel">
        <div class="x_content">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Merchant Name</th>
                    <th class="text-center"># Address</th>
                    <th class="text-center">Logo</th>
                    <th class="text-center">Enable</th>
                    <th class="col-sm-1 text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($merchants as $key => $merchant)
                    <tr>
                        <td>{!! (($merchants->currentPage() - 1) * $merchants->perPage()) + $key + 1 !!}</td>
                        <td>{!! $merchant->merchant_name !!}</td>
                        <td class="text-center"><a class="badge badge-primary" href="{!! action('Admin\MerchantController@edit', $merchant->uuid) . '#address' !!}">{!! count($merchant->addresses) !!}</a></td>
                        <td class="text-center"><img class="img-thumbnail" src="{!! ImageHelper::tinyHeighten($merchant->logo) !!}"></td>
                        <td class="text-center">
                            @if($merchant->is_enabled)
                                <i class="fa fa-check-circle fa-lg text-success" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-times-circle fa-lg text-danger" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td class="nowrap">
                            <a href="{!! action('Admin\MerchantController@edit', $merchant->uuid) !!}" class="{!! $button['edit'] !!} btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                            {!! Form::open(['class' => 'form-delete', 'url' => action('Admin\MerchantController@destroy', $merchant->uuid), 'method' => 'DELETE']) !!}
                            {!! Form::hidden('_method', 'delete') !!}
                            <button type="submit" class="{!! $button['delete'] !!} btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $merchants->links() !!}
            </div>
        </div>
    </div>
@endsection

@section('styles_before')
    @parent
@endsection

@section('footer_scripts')
    @parent
@endsection

