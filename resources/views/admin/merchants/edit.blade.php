@extends('admin.layouts.master_admin')

@section('title', 'Edit Merchant')

@section('content')
    @include('admin.merchants.form',[
      'form_url' => action('Admin\MerchantController@update', $merchant->uuid),
      'method' => 'put',
      'button_text' => 'Update Merchant'
    ])
@endsection