@extends('admin.layouts.master_admin')

@section('title', 'Create Merchant')

@section('content')
    @include('admin.merchants.form',[
      'form_url' => action('Admin\MerchantController@store'),
      'method' => 'post',
      'button_text' => 'Create Merchant'
    ])
@endsection