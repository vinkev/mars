<div class="row">
    <div class="col-sm-12">
        <div class="x_panel">
            <div class="x_title clearfix">
                <h2>Merchant Info</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
            </div>
            <div class="x_content">
                {!! Form::open(['class' => 'form-horizontal', 'url' => $form_url, 'method' => $method, 'files' => true]) !!}
                <div class="form-group">
                    <label class="control-label col-sm-3" for="is_enabled">Is Enabled</label>
                    <div class="col-sm-6">
                        <div class="checkbox">
                            {!! Form::checkbox('is_enabled', 1, $merchant->is_enabled, ['class' => 'switchery form-control', 'checked' => 'checked']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="merchant_name">Merchant Name</label>
                    <div class="col-sm-6">
                        {!! Form::text('merchant_name', $merchant->merchant_name, ['class' => 'form-control' . ($errors->has('merchant_name') ? ' parsley-error' : '') , 'placeholder' => 'Merchant Name', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'merchant_name'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="logo">Logo</label>
                    <div class="col-sm-6">
                        {!! Form::file('logo', ['class' => 'file-upload' . ($errors->has('logo') ? ' parsley-error' : ''), 'data-max-file-size' => config('domain.general.max_upload.merchant.logo'), 'data-allowed-file-types' => '["image"]', 'data-initial-preview' => !empty($merchant->logo) ? '["' . asset('storage/' . $merchant->logo) . '"]' : '']) !!}
                        {!! Form::hidden('logo_path', $merchant->logo, ['class' => 'file-upload-path']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'logo'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="website">Website</label>
                    <div class="col-sm-6">
                        {!! Form::text('website', $merchant->website, ['class' => 'form-control' . ($errors->has('website') ? ' parsley-error' : '') , 'placeholder' => 'Website', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'website'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="description">Description</label>
                    <div class="col-sm-6">
                        {!! Form::textarea('description', $merchant->description, ['class' => 'form-control' . ($errors->has('description') ? ' parsley-error' : ''), 'placeholder' => 'Description']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'description'])
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="{!! $button['submit'] !!} w-md" name="submit">{!! $button_text !!}</button>
                        <a href="{!! action('Admin\MerchantController@index') !!}" class="{!! $button['cancel'] !!} w-md">Cancel</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@if($method == 'put')
    @include('admin.addresses.index', [
        'model' => $merchant,
        'subtitle' => 'Merchant Addresses List'
    ])
@endif

@section('styles_before')
    @parent

    {!! Html::style(mix('assets/admin/css/edit.css')) !!}
@endsection

@section('footer_scripts')
    @parent

    {!! Html::script(mix('assets/admin/js/edit.js')) !!}

    @if($method == 'put')
        <script type="text/javascript">
            $(document).ready(function () {
                $('.collapse-link').trigger('click');
            });
        </script>
    @endif
@endsection
