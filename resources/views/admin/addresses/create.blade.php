@extends('admin.layouts.master_admin')

@section('title', 'Create Address')

@section('content')
    @include('admin.addresses.form',[
      'form_url' => route('admin.'.$route_prefix.'.addresses.store', $polymorphic_model->uuid),
      'method' => 'post',
      'button_text' => 'Create Address'
    ])
@endsection