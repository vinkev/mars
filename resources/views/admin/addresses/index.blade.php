<div class="row" id="address">
    <div class="col-sm-12">
        <div class="x_panel">
            <div class="x_title clearfix">
                <h2>{!! $subtitle !!}</h2>
                <div class="pull-right">
                    <div class="pull-right">
                        <a href="{!! route('admin.' . $route_prefix . '.addresses.create', [$model->uuid]) !!}" class="{!! $button['create'] !!} btn-sm w-md"><i class="fa fa-plus-square" aria-hidden="true"></i> Create New Address</a>
                    </div>
                </div>
            </div>
            <div class="x_content">
                <div class="row">
                    <div class="col-sm-12">
                        <table class="table table-striped table-hover table-responsive">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Location Name</th>
                                <th>Location Info</th>
                                <th>Address</th>
                                <th class="text-center">Enable</th>
                                <th class="col-sm-1 text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($model->addresses as $key => $address)
                                <tr>
                                    <td>{!! $key + 1 !!}</td>
                                    <td>{!! $address->location_name !!}</td>
                                    <td>{!! $address->location_info !!}</td>
                                    <td>
                                        {!! nl2br($address->address) !!}{!! ', ' . $address->city !!}
                                        @if(!empty($address->state) || !empty($address->zip_code))
                                            <br>
                                            {!! !empty($address->state) ? $states[$address->state]  : '' !!}{!! !empty($address->zip_code) ? ', ' . $address->zip_code  : '' !!}
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($address->is_enabled)
                                            <i class="fa fa-check-circle fa-lg text-success" aria-hidden="true"></i>
                                        @else
                                            <i class="fa fa-times-circle fa-lg text-danger" aria-hidden="true"></i>
                                        @endif
                                    </td>
                                    <td class="nowrap">
                                        <a href="{!! route('admin.' . $route_prefix . '.addresses.edit', [$merchant->uuid, $address->uuid]) !!}" class="{!! $button['edit'] !!} btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                        {!! Form::open(['class' => 'form-delete', 'url' => route('admin.' . $route_prefix . '.addresses.destroy', [$merchant->uuid, $address->uuid]), 'method' => 'DELETE']) !!}
                                        {!! Form::hidden('_method', 'delete') !!}
                                        <button type="submit" class="{!! $button['delete'] !!} btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>