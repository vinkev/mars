@extends('admin.layouts.master_admin')

@section('title', 'Edit Address')

@section('content')
    @include('admin.addresses.form',[
      'form_url' => route('admin.'.$route_prefix.'.addresses.update', [$polymorphic_model->uuid, $address->uuid]),
      'method' => 'put',
      'button_text' => 'Update Address'
    ])
@endsection