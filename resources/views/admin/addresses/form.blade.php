<div class="row">
    <div class="col-sm-12">
        <div class="x_panel">
            <div class="x_content">
                {!! Form::open(['class' => 'form-horizontal', 'url' => $form_url, 'method' => $method]) !!}
                <div class="form-group">
                    <label class="control-label col-sm-3" for="is_enabled">Is Enabled</label>
                    <div class="col-sm-6">
                        <div class="checkbox">
                            {!! Form::checkbox('is_enabled', 1, $address->is_enabled, ['class' => 'switchery form-control', 'checked' => 'checked']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="location_name">Location Name / Building Name</label>
                    <div class="col-sm-6">
                        {!! Form::text('location_name', $address->location_name, ['class' => 'form-control' . ($errors->has('location_name') ? ' parsley-error' : '') , 'placeholder' => 'Location Name', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'location_name'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="location_info">Location Info / Floor Info</label>
                    <div class="col-sm-6">
                        {!! Form::text('location_info', $address->location_info, ['class' => 'form-control' . ($errors->has('location_info') ? ' parsley-error' : '') , 'placeholder' => 'Location Info', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'location_info'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="address">Address</label>
                    <div class="col-sm-6">
                        {!! Form::textarea('address', $address->address, ['class' => 'form-control' . ($errors->has('address') ? ' parsley-error' : '') , 'placeholder' => 'Address', 'autocomplete' => 'off', 'rows' => 4]) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'address'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="city">City</label>
                    <div class="col-sm-6">
                        {!! Form::text('city', $address->city, ['class' => 'form-control' . ($errors->has('city') ? ' parsley-error' : '') , 'placeholder' => 'City', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'city'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="state">State</label>
                    <div class="col-sm-6">
                        {!! Form::select('state', $states, $address->state, ['class' => 'form-control' . ($errors->has('state') ? ' parsley-error' : '') , 'data-placeholder' => 'Choose State', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'state'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="zip_code">Zip Code</label>
                    <div class="col-sm-2">
                        {!! Form::text('zip_code', $address->zip_code, ['class' => 'form-control' . ($errors->has('zip_code') ? ' parsley-error' : '') , 'placeholder' => 'Zip Code', 'autocomplete' => 'off']) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-sm-offset-3 col-sm-9">
                        @include('admin.layouts.form_error', ['field_name' => 'zip_code'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="office_phone">Office Phone</label>
                    @for($i = 0; $i <= 3; $i++)
                        <div class="col-sm-3 {!! $i > 0 ? 'col-sm-offset-3' : '' !!}">
                            {!! Form::text('office_phone[]', !empty($address->office_phone[$i]) ? $address->office_phone[$i] : '' , ['class' => 'form-control' . ($errors->has('office_phone.' . $i) ? ' parsley-error' : '') , 'placeholder' => 'Office Phone', 'autocomplete' => 'off']) !!}
                        </div>
                        <div class="col-sm-6">
                            @include('admin.layouts.form_error', ['field_name' => 'office_phone.' . $i])
                        </div>
                        <div class="clearfix"></div>
                    @endfor
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="fax_phone">Fax Phone</label>
                    @for($i = 0; $i <=3; $i++)
                        <div class="col-sm-3 {!! $i > 0 ? 'col-sm-offset-3' : '' !!}">
                            {!! Form::text('fax_phone[]', !empty($address->fax_phone[$i]) ? $address->fax_phone[$i] : '', ['class' => 'form-control' . ($errors->has('fax_phone.' . $i) ? ' parsley-error' : '') , 'placeholder' => 'Fax Phone', 'autocomplete' => 'off']) !!}
                        </div>
                        <div class="col-sm-6">
                            @include('admin.layouts.form_error', ['field_name' => 'fax_phone.' . $i])
                        </div>
                        <div class="clearfix"></div>
                    @endfor
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="{!! $button['submit'] !!} w-md" name="submit">{!! $button_text !!}</button>
                        <a href="{!! $polymorphic_edit_route !!}" class="{!! $button['cancel'] !!} w-md">Cancel</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('styles_before')
    @parent

    {!! Html::style(mix('assets/admin/css/edit.css')) !!}
@endsection

@section('footer_scripts')
    @parent

    {!! Html::script(mix('assets/admin/js/edit.js')) !!}
    <script type="text/javascript">
        $("optgroup").on("click", function() {
            $(this).children("option").prop("selected", "selected");
            $(this).next().children("option").prop("selected", false);
            $(this).prev().children("option").prop("selected", false);
        });
    </script>
@endsection