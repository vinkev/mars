@extends('admin.layouts.master_admin')

@section('title', 'Promos List')

@section('title-right')
    <div class="pull-right">
        <a href="{!! action('Admin\PromoController@create') !!}" class="{!! $button['create'] !!} w-md"><i class="fa fa-plus-square" aria-hidden="true"></i> Create New Promo</a>
    </div>
@endsection

@section('content')
    <div class="x_panel">
        <div class="x_content">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Promo Title</th>
                    <th>Merchant</th>
                    <th>Bank</th>
                    <th class="text-center">Card</th>
                    <th class="text-center">Image</th>
                    <th>Publish at</th>
                    <th>Period</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Enable</th>
                    <th class="col-sm-1 text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($promos as $key => $promo)
                    <tr>
                        <td>{!! (($promos->currentPage() - 1) * $promos->perPage()) + $key + 1 !!}</td>
                        <td>{!! $promo->promo_title !!}</td>
                        <td>{!! $promo->merchant->merchant_name !!}</td>
                        <td>
                            @foreach($promo->banks as $bank)
                                <span class="label label-primary">{!! $bank->bank_nickname !!}</span>
                            @endforeach
                        </td>
                        <td>
                            <span class="badge" data-toggle="popover" data-html="true" data-placement="right" data-content="
                            <ol class='number-popover'>
                            @foreach($promo->cards as $card)
                                    <li>{!! $card->card_name !!}</li>
                            @endforeach
                                    </ol>">{!! $promo->cards->count() !!}</span>
                        </td>
                        <td class="text-center">
                            <a class="pointer" data-toggle="modal" data-target="#image-modal" data-image-large="{!! ImageHelper::large($promo->image) !!}">
                                <img class="img-thumbnail img-responsive" src="{!! ImageHelper::tinyHeighten($promo->image) !!}">
                            </a>
                        </td>
                        <td>{!! $promo->publish_at->toDateString() !!}</td>
                        <td>{!! $promo->start_at->toDateString() . ' - ' . $promo->end_at->toDateString() !!}</td>
                        <td class="text-center">
                            @if($promo->isActive)
                                <span class="label label-success">Active</span>
                            @elseif ($promo->isComingSoon)
                                <span class="label label-warning">Soon</span>
                            @elseif ($promo->isExpired)
                                <span class="label label-default">Expired</span>
                            @endif
                        </td>
                        <td class="text-center">
                            @if($promo->is_enabled)
                                <i class="fa fa-check-circle fa-lg text-success" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-times-circle fa-lg text-danger" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="{!! action('Admin\PromoController@edit', $promo->uuid) !!}" class="{!! $button['edit'] !!} btn-xs w-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                            {!! Form::open(['class' => 'form-delete', 'url' => action('Admin\PromoController@destroy', $promo->uuid), 'method' => 'DELETE']) !!}
                            {!! Form::hidden('_method', 'delete') !!}
                            <button type="submit" class="{!! $button['delete'] !!} btn-xs w-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $promos->links() !!}
            </div>
        </div>
    </div>

    @include('admin.layouts.modal_image')
@endsection

@section('styles_before')
    @parent
@endsection

@section('footer_scripts')
    @parent

    <script type="text/javascript">
        $('[data-toggle="popover"]').popover({
            trigger: 'hover'
        });

        $('#image-modal').on('show.bs.modal', function (e) {
            $('#image-modal .modal-body .loading').removeClass('hide');
            $('#image-modal .modal-body .image-preview').remove();

            var image_path = $(e.relatedTarget).data('image-large');
            var image = new Image();
            image.onload =function(){
              $('#image-modal .modal-body .loading').addClass('hide');
              $('#image-modal .modal-body').append('<img class="image-preview" src="' + image_path + '">');
              $('#image-modal .modal-body .image-preview').hide().fadeIn();
            };
            image.src = image_path;
        });
    </script>
@endsection

