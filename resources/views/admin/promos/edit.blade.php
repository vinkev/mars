@extends('admin.layouts.master_admin')

@section('title', 'Edit Promo')

@section('content')
    @include('admin.promos.form',[
      'form_url' => action('Admin\PromoController@update', $promo->uuid),
      'method' => 'put',
      'button_text' => 'Update Promo'
    ])
@endsection