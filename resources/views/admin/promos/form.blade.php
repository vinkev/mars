<div class="row">
    <div class="col-sm-12">
        <div class="x_panel">
            <div class="x_content">
                {!! Form::open(['class' => 'form-horizontal', 'url' => $form_url, 'method' => $method, 'files' => true]) !!}
                <div class="form-group">
                    <label class="control-label col-sm-3" for="is_enabled">Is Enabled</label>
                    <div class="col-sm-6">
                        <div class="checkbox">
                            {!! Form::checkbox('is_enabled', 1, $promo->is_enabled, ['class' => 'switchery form-control', 'checked' => 'checked']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="promo_title">Promo Title</label>
                    <div class="col-sm-6">
                        {!! Form::text('promo_title', $promo->promo_title, ['class' => 'form-control' . ($errors->has('promo_title') ? ' parsley-error' : '') , 'placeholder' => 'Promo Title', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'promo_title'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="promo_title">Promo Subtitle</label>
                    <div class="col-sm-6">
                        {!! Form::text('promo_subtitle', $promo->promo_subtitle, ['class' => 'form-control' . ($errors->has('promo_subtitle') ? ' parsley-error' : '') , 'placeholder' => 'Promo Subtitle', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'promo_subtitle'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="merchant">Merchant</label>
                    <div class="col-sm-6">
                        {!! Form::select('merchant', $merchants, $promo->merchant->uuid, ['class' => 'form-control' . ($errors->has('merchant') ? ' parsley-error' : ''), 'data-placeholder' => 'Choose Merchant', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'merchant'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="payment_method">Payment Method</label>
                    <div class="col-sm-6">
                        <select id="payment_method" class="form-control {!! $errors->has('payment_method') ? ' parsley-error' : '' !!}" name="payment_method[]" autocomplete="off" multiple="multiple" data-close-on-select="false" data-select-on-close="false" data-allow-clear="true" data-placeholder="Choose Payment Method">
                            @foreach($grouped_cards as $bank => $grouped_card)
                                <optgroup label="{!! $bank !!}">
                                    @foreach($grouped_card as $card)
                                        <option value="{!! $card->uuid !!}" {!! $promo->isCardSelected($card->id) ? 'selected' : '' !!} >{!! $card->card_name !!}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                        @include('admin.layouts.form_error', ['field_name' => 'payment_method'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="publish_at">Publish At</label>
                    <div class="col-sm-2">
                        <div class="input-append input-group">
                            {!! Form::text('publish_at', $promo->publish_at, ['class' => 'form-control datepicker' . ($errors->has('publish_at') ? ' parsley-error' : '') , 'placeholder' => 'Publish At', 'autocomplete' => 'off', 'readonly' => 'readonly']) !!}
                            <div class="add-on input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                        @include('admin.layouts.form_error', ['field_name' => 'publish_at'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="start_at">Start At</label>
                    <div class="col-sm-2">
                        <div class="input-append input-group">
                            {!! Form::text('start_at', $promo->start_at, ['class' => 'form-control datepicker start-at' . ($errors->has('start_at') ? ' parsley-error' : '') , 'placeholder' => 'Start At', 'autocomplete' => 'off', 'readonly' => 'readonly']) !!}
                            <div class="add-on input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                        @include('admin.layouts.form_error', ['field_name' => 'start_at'])
                    </div>
                    <label class="control-label col-sm-1" for="end_at">End At</label>
                    <div class="col-sm-2">
                        <div class="input-append input-group">
                            {!! Form::text('end_at', $promo->end_at, ['class' => 'form-control datepicker end-at' . ($errors->has('start_at') ? ' parsley-error' : '') , 'placeholder' => 'End At', 'autocomplete' => 'off', 'readonly' => 'readonly']) !!}
                            <div class="add-on input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                        </div>
                        @include('admin.layouts.form_error', ['field_name' => 'end_at'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="image">Image</label>
                    <div class="col-sm-6">
                        {!! Form::file('image', ['class' => 'file-upload' . ($errors->has('image') ? ' parsley-error' : ''), 'data-max-file-size' => config('domain.general.max_upload.promo.image'), 'data-allowed-file-types' => '["image"]', 'data-initial-preview' => !empty($promo->image) ? '["' . asset('storage/' . $promo->image) . '"]' : '']) !!}
                        {!! Form::hidden('image_path', $promo->image, ['class' => 'file-upload-path']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'image'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="description">Description</label>
                    <div class="col-sm-6">
                        {!! Form::textarea('description', $promo->description, ['class' => 'form-control wysiwyg' . ($errors->has('description') ? ' parsley-error' : ''), 'placeholder' => 'Description']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'description'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="term_condition">Term & Condition</label>
                    <div class="col-sm-6">
                        {!! Form::textarea('term_condition', $promo->term_condition, ['class' => 'form-control wysiwyg' . ($errors->has('term_condition') ? ' parsley-error' : ''), 'placeholder' => 'Term & Condition']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'term_condition'])
                    </div>
                </div>

                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="{!! $button['submit'] !!} w-md" name="submit">{!! $button_text !!}</button>
                        <a href="{!! action('Admin\PromoController@index') !!}" class="{!! $button['cancel'] !!} w-md">Cancel</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('styles_before')
    @parent

    {!! Html::style(mix('assets/admin/css/edit.css')) !!}
@endsection

@section('footer_scripts')
    @parent

    {!! Html::script(mix('assets/admin/js/edit.js')) !!}
@endsection
