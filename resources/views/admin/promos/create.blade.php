@extends('admin.layouts.master_admin')

@section('title', 'Create Promo')

@section('content')
    @include('admin.promos.form',[
      'form_url' => action('Admin\PromoController@store'),
      'method' => 'post',
      'button_text' => 'Create Promo'
    ])
@endsection