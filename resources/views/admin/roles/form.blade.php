<div class="row">
    <div class="col-sm-12">
        <div class="x_panel">
            <div class="x_content">
                {{ Form::open(['class' => 'form-horizontal', 'url' => $form_url, 'method' => $method]) }}
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="code">Code</label>
                    <div class="col-sm-6">
                        {{ Form::text('code', $role->name, ['class' => 'form-control' . ($errors->has('code') ? ' parsley-error' : '') , 'placeholder' => 'Code']) }}
                        @include('admin.layouts.form_error', ['field_name' => 'code'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="title">Title</label>
                    <div class="col-sm-6">
                        {{ Form::text('title', $role->title, ['class' => 'form-control' . ($errors->has('title') ? ' parsley-error' : ''), 'placeholder' => 'Title']) }}
                        @include('admin.layouts.form_error', ['field_name' => 'title'])
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="{!! $button['submit'] !!} w-md" name="submit">{!! $button_text !!}</button>
                        <a href="{!! action('Admin\RoleController@index') !!}" class="{!! $button['cancel'] !!} w-md">Cancel</a>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>