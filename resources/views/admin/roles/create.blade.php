@extends('admin.layouts.master_admin')

@section('title', 'Create Role')

@section('content')
    @include('admin.roles.form',[
      'form_url' => action('Admin\RoleController@store'),
      'method' => 'post',
      'button_text' => 'Create Role'
    ])
@endsection