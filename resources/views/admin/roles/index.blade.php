@extends('admin.layouts.master_admin')

@section('title', 'Roles List')

@section('title-right')
    <div class="pull-right">
        <a href="{!! action('Admin\RoleController@create') !!}" class="{!! $button['create'] !!} w-md"><i class="fa fa-plus-square" aria-hidden="true"></i> Create New Role</a>
    </div>
@endsection

@section('content')
    <div class="x_panel">
        <div class="x_content">

            <table class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th>Code</th>
                    <th>Title</th>
                    <th class="col-sm-1 text-center">Action</th>
                </tr>
                </thead>
                <tbody>

                @foreach($roles as $key => $role)
                    <tr>
                        <td>{!! $role->name !!}</td>
                        <td>{!! $role->title !!}</td>
                        <td class="nowrap">
                            <a href="{!! action('Admin\RoleController@edit', $role->id) !!}" class="{!! $button['edit'] !!} btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                            {!! Form::open(['class' => 'form-delete', 'url' => action('Admin\RoleController@destroy', $role->id), 'method' => 'DELETE']) !!}
                            {!! Form::hidden('_method', 'delete') !!}
                            <button type="submit" class="{!! $button['delete'] !!} btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
            {!! $roles->links() !!}
            </div>
        </div>
    </div>
@endsection

@section('styles_before')
    @parent
@endsection

@section('footer_scripts')
    @parent
@endsection

