@extends('admin.layouts.master_admin')

@section('title', 'Edit Role')

@section('content')
    @include('admin.roles.form',[
      'form_url' => action('Admin\RoleController@update', $role->id),
      'method' => 'put',
      'button_text' => 'Update Role'
    ])
@endsection