@extends('admin.layouts.master_admin')

@section('title', 'Create User')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right">
                <a href="{!! action('Admin\UserController@create') !!}" class="{!! $button['create'] !!} w-md"><i class="fa fa-plus-square" aria-hidden="true"></i> Create</a>
            </div>
        </div>
    </div>
    <table class="table table-striped table-hover table-responsive">
        <thead>
        <tr>
            <th>#</th>
            <th>Email</th>
            <th>Name</th>
            <th>Roles</th>
            <th>Status</th>
            <th>Created At</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td></td>
                <td>{!! $user->email !!}</td>
                <td>{!! $user->profile->name !!}</td>
                <td></td>
                <td>{!! $user->status !!}</td>
                <td>{!! $user->created_at !!}</td>
                <td><a href="{!! action('Admin\UserController@edit', $user->uuid) !!}" class="{!! $button['edit'] !!} btn-xs w-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('styles_before')
    @parent
@endsection

@section('footer_scripts')
    @parent
@endsection

