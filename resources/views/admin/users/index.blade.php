@extends('admin.layouts.master_admin')

@section('title', 'User List')

@section('title-right')
    <div class="pull-right">
        <a href="{!! action('Admin\UserController@create') !!}" class="{!! $button['create'] !!} w-md"><i class="fa fa-plus-square" aria-hidden="true"></i> Create New User</a>
    </div>
@endsection

@section('content')
    <div class="x_panel">
        <div class="x_content">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Email</th>
                    <th>Name</th>
                    <th>Roles</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th class="col-sm-1 text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $key => $user)
                    <tr>
                        <td>{!! $users->currentPage() + $key !!}</td>
                        <td>{!! $user->email !!}</td>
                        <td>{!! $user->profile->name !!}</td>
                        <td>
                            @foreach($user->roles as $role)
                                <span class="label label-default">{!! $role->name !!}</span>
                            @endforeach
                        </td>
                        <td>
                            @if($user->user_status == Mars\Helpers\Constants\UserStatus::PENDING)
                                <span class="label label-warning">{!! $user_statuses[$user->user_status] !!}</span>
                            @elseif($user->user_status == Mars\Helpers\Constants\UserStatus::ACTIVE)
                                <span class="label label-success">{!! $user_statuses[$user->user_status] !!}</span>
                            @elseif($user->user_status == Mars\Helpers\Constants\UserStatus::INACTIVE)
                                <span class="label label-default">{!! $user_statuses[$user->user_status] !!}</span>
                            @elseif($user->user_status == Mars\Helpers\Constants\UserStatus::BANNED)
                                <span class="label label-danger">{!! $user_statuses[$user->user_status] !!}</span>
                            @endif
                        </td>
                        <td>{!! $user->created_at !!}</td>
                        <td><a href="{!! action('Admin\UserController@edit', $user->uuid) !!}" class="{!! $button['edit'] !!} btn-xs w-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $users->links() !!}
            </div>
        </div>
    </div>
@endsection


