@extends('layouts.master')

@section('body_class','nav-md')

@section('page')
    <div class="container body">
        <div class="main_container clearfix">
            @section('header')
                @include('admin.sections.navigation')
                @include('admin.sections.header')
            @show

            @yield('left-sidebar')

            <div class="right_col" role="main">
                <div class="page-title">
                    <div class="title_left">
                        <h1 class="title">@yield('title')</h1>
                        @if(Breadcrumbs::exists())
                            {!! Breadcrumbs::render() !!}
                        @endif
                    </div>
                    <div class="title_right">
                        @yield('title-right')
                    </div>
                </div>
                @yield('content')
            </div>

            <footer>
                @include('admin.sections.footer')
            </footer>
        </div>
    </div>
@stop

@section('styles_after')
    @parent

    {{ Html::style(mix('assets/admin/css/admin.css')) }}
@endsection

@section('footer_scripts')
    @parent
    {{ Html::script(mix('assets/admin/js/admin.js')) }}

    <script type="text/javascript">
        @if(!empty($notification))
        new PNotify({
            title: '{!! title_case($notification->type) !!}',
            text: '{!! $notification->message !!}',
            type: '{!! $notification->type !!}',
            styling: 'brighttheme',
            animate: {
                animate: true,
                in_class: 'slideInDown',
                out_class: 'slideOutUp'
            },
            nonblock: {
                nonblock: true
            },
            buttons: {
                show_on_nonblock: true
            }
        });
        @endif
    </script>
@endsection