@if($errors->has($field_name))
    <ul class="parsley-errors-list filled">
        @foreach($errors->get($field_name) as $error)
            <li class="parsley-required">{{ $error }}</li>
        @endforeach
    </ul>
@endif