<div class="top_nav clearfix">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                       aria-expanded="false">
                        @if(!empty($self->profile->photo))
                            <img src="{{ $self->profile->photo }}" alt="{{ $self->profile->name }}">
                        @else
                        @endif

                        {{ $self->profile->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li>
                            <a href="{{ action('Auth\LoginController@logout') }}">
                                <i class="fa fa-sign-out pull-right"></i> Logout
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>