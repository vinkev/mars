<div class="clearfix">
    <div class="pull-right">
        &copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved
    </div>
</div>