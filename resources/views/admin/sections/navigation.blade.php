<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{!! action('Admin\DashboardController@index') !!}" class="site_title">
                <span class="fa-stack fa-md">
                    <i class="fa fa-square-o  fa-stack-2x"></i>
                    <i class="fa fa-shopping-bag fa-stack-1x"></i>
                </span>
                <span>{!! config('app.name') !!}</span>
            </a>
        </div>

        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        {{--<div class="profile clearfix">--}}
            {{--@if($self->profile->photo)--}}
                {{--<div class="profile_pic">--}}
                    {{--<img src="{!! $self->profile->photo !!}" alt="..." class="img-circle profile_img">--}}
                {{--</div>--}}
            {{--@endif--}}
            {{--<div class="profile_info {!! $self->profile->photo ? '' : 'pull-right' !!}">--}}
                {{--<h2>{!! $self->profile->name !!}</h2>--}}
            {{--</div>--}}
        {{--</div>--}}
        <!-- /menu profile quick info -->

        <br/>

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    <li class="{!! request()->segment(2) == 'dashboard' ? 'active' : '' !!}">
                        <a href="{!! action('Admin\DashboardController@index') !!}"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a>
                    </li>
                    <li class="{!! request()->segment(2) == 'promos' ? 'active' : '' !!}">
                        <a href="{!! action('Admin\PromoController@index') !!}"><i class="fa fa-certificate" aria-hidden="true"></i> Promo</a>
                    </li>
                    <li class="{!! request()->segment(2) == 'users' ? 'active' : '' !!}">
                        <a href="{!! action('Admin\UserController@index') !!}"><i class="fa fa-users" aria-hidden="true"></i> Users</a>
                    </li>
                    <li class="{!! in_array(request()->segment(2), ['roles', 'cards', 'card-brands', 'banks', 'merchants', 'permissions']) ? 'active' : '' !!}">
                        <a><i class="fa fa-archive" aria-hidden="true"></i> Data Source <span class="fa fa-chevron-down"></span></a>

                        <ul class="nav child_menu">
                            <li class="{!! request()->segment(2) == 'cards' ? 'current-page' : '' !!}">
                                <a href="{!! action('Admin\CardController@index') !!}"><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Cards</a>
                            </li>
                            <li class="{!! request()->segment(2) == 'banks' ? 'current-page' : '' !!}">
                                <a href="{!! action('Admin\BankController@index') !!}"><i class="fa fa-university" aria-hidden="true"></i> Banks</a>
                            </li>
                            <li class="{!! request()->segment(2) == 'card-brands' ? 'current-page' : '' !!}">
                                <a href="{!! action('Admin\CardBrandController@index') !!}"><i class="fa fa-cc-visa" aria-hidden="true"></i> Card Brands</a>
                            </li>
                            <li class="{!! request()->segment(2) == 'merchants' ? 'current-page' : '' !!}">
                                <a href="{!! action('Admin\MerchantController@index') !!}"><i class="fa fa-building-o" aria-hidden="true"></i> Merchants</a>
                            </li>
                            <li class="{!! request()->segment(2) == 'roles' ? 'current-page' : '' !!}">
                                <a href="{!! action('Admin\RoleController@index') !!}"><i class="fa fa-gavel" aria-hidden="true"></i> Roles</a>
                            </li>
                            <li class="{!! request()->segment(2) == 'permissions' ? 'current-page' : '' !!}">
                                <a><i class="fa fa-handshake-o" aria-hidden="true"></i> Permissions</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
