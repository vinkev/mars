<div class="row">
    <div class="col-sm-12">
        <div class="x_panel">
            <div class="x_content">
                {!! Form::open(['class' => 'form-horizontal', 'url' => $form_url, 'method' => $method, 'files' => true]) !!}
                <div class="form-group">
                    <label class="control-label col-sm-3" for="is_enabled">Is Enabled</label>
                    <div class="col-sm-6">
                        <div class="checkbox">
                            {!! Form::checkbox('is_enabled', 1, $card->is_enabled, ['class' => 'switchery form-control', 'checked' => 'checked']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="bank">Bank</label>
                    <div class="col-sm-6">
                        {!! Form::select('bank', $banks, $card->bank->uuid, ['class' => 'form-control' . ($errors->has('card_name') ? ' parsley-error' : ''), 'data-placeholder' => 'Choose Bank', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'bank'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="card_brand">Card Brand</label>
                    <div class="col-sm-6">
                        <select name="card_brand" id="card_brand" class="form-control select2-custom select2-image {!! $errors->has('card_brand') ? 'parsley-error' : '' !!}">
                            <option>Choose Card Brand</option>
                            @foreach($card_brands as $card_brand)
                                <option value="{!! $card_brand->uuid !!}" data-image="{!! ImageHelper::tinyHeighten($card_brand->logo) !!}" {!! $card_brand->uuid == request()->old('card_brand', $card->brand->uuid) ? 'selected' : '' !!}>{!! $card_brand->brand_name !!}</option>
                            @endforeach
                        </select>
                        @include('admin.layouts.form_error', ['field_name' => 'card_brand'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="name">Card Name</label>
                    <div class="col-sm-6">
                        {!! Form::text('card_name', $card->card_name, ['class' => 'form-control' . ($errors->has('card_name') ? ' parsley-error' : '') , 'placeholder' => 'Card Name', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'card_name'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="card_type">Card Type</label>
                    <div class="col-sm-6">
                        {!! Form::select('card_type', $card_types, $card->card_type, ['class' => 'form-control' . ($errors->has('card_ty[e') ? ' parsley-error' : ''), 'data-placeholder' => 'Choose Card Type', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'card_type'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="image">Image</label>
                    <div class="col-sm-6">
                        {!! Form::file('image', ['class' => 'file-upload' . ($errors->has('image') ? ' parsley-error' : ''), 'data-max-file-size' => config('domain.general.max_upload.card.image'), 'data-allowed-file-types' => '["image"]', 'data-initial-preview' => !empty($card->image) ? '["' . asset('storage/' . $card->image) . '"]' : '']) !!}
                        {!! Form::hidden('image_path', $card->image, ['class' => 'file-upload-path']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'image'])
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="{!! $button['submit'] !!} w-md" name="submit">{!! $button_text !!}</button>
                        <a href="{!! action('Admin\CardController@index') !!}" class="{!! $button['cancel'] !!} w-md">Cancel</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('styles_before')
    @parent

    {!! Html::style(mix('assets/admin/css/edit.css')) !!}
@endsection

@section('footer_scripts')
    @parent
    {!! Html::script(mix('assets/admin/js/edit.js')) !!}
@endsection
