@extends('admin.layouts.master_admin')

@section('title', 'Create Card')

@section('content')
    @include('admin.cards.form',[
      'form_url' => action('Admin\CardController@store'),
      'method' => 'post',
      'button_text' => 'Create Card'
    ])
@endsection