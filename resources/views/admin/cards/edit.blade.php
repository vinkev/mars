@extends('admin.layouts.master_admin')

@section('title', 'Edit Card')

@section('content')
    @include('admin.cards.form',[
      'form_url' => action('Admin\CardController@update', $card->uuid),
      'method' => 'put',
      'button_text' => 'Update Card'
    ])
@endsection
