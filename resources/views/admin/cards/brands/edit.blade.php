@extends('admin.layouts.master_admin')

@section('title', 'Edit Card Brand')

@section('content')
    @include('admin.cards.brands.form',[
      'form_url' => action('Admin\CardBrandController@update', $card_brand->uuid),
      'method' => 'put',
      'button_text' => 'Update Card Brand'
    ])
@endsection
