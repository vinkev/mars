@extends('admin.layouts.master_admin')

@section('title', 'Create Card Brand')

@section('content')
    @include('admin.cards.brands.form',[
      'form_url' => action('Admin\CardBrandController@store'),
      'method' => 'post',
      'button_text' => 'Create Card Brand'
    ])
@endsection