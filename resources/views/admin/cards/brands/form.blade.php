<div class="row">
    <div class="col-sm-12">
        <div class="x_panel">
            <div class="x_content">
                {!! Form::open(['class' => 'form-horizontal', 'url' => $form_url, 'method' => $method, 'files' => true]) !!}
                <div class="form-group">
                    <label class="control-label col-sm-3" for="is_enabled">Is Enabled</label>
                    <div class="col-sm-6">
                        <div class="checkbox">
                            {!! Form::checkbox('is_enabled', 1, $card_brand->is_enabled, ['class' => 'switchery form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="name">Brand Name</label>
                    <div class="col-sm-6">
                        {!! Form::text('brand_name', $card_brand->brand_name, ['class' => 'form-control' . ($errors->has('brand_name') ? ' parsley-error' : '') , 'placeholder' => 'Brand Name', 'autocomplete' => 'off']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'brand_name'])
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label col-sm-3" for="logo">Logo</label>
                    <div class="col-sm-6">
                        {!! Form::file('logo', ['class' => 'file-upload' . ($errors->has('logo') ? ' parsley-error' : ''), 'data-max-file-size' => config('domain.general.max_upload.brand.logo'), 'data-allowed-file-types' => '["image"]', 'data-initial-preview' => !empty($card_brand->logo) ? '["' . asset('storage/' . $card_brand->logo) . '"]' : '']) !!}
                        {!! Form::hidden('logo_path', $card_brand->logo, ['class' => 'file-upload-path']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'logo'])
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="description">Description</label>
                    <div class="col-sm-6">
                        {!! Form::textarea('description', $card_brand->description, ['class' => 'form-control' . ($errors->has('description') ? ' parsley-error' : ''), 'placeholder' => 'Description']) !!}
                        @include('admin.layouts.form_error', ['field_name' => 'description'])
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="{!! $button['submit'] !!} w-md" name="submit">{!! $button_text !!}</button>
                        <a href="{!! action('Admin\CardBrandController@index') !!}" class="{!! $button['cancel'] !!} w-md">Cancel</a>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('styles_before')
    @parent

    {!! Html::style(mix('assets/admin/css/edit.css')) !!}
@endsection

@section('footer_scripts')
    @parent

    {!! Html::script(mix('assets/admin/js/edit.js')) !!}
@endsection
