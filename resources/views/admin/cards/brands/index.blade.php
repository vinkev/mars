@extends('admin.layouts.master_admin')

@section('title', 'Card Brands List')

@section('title-right')
    <div class="pull-right">
        <a href="{!! action('Admin\CardBrandController@create') !!}" class="{!! $button['create'] !!} w-md"><i class="fa fa-plus-square" aria-hidden="true"></i> Create New Card Brand</a>
    </div>
@endsection

@section('content')
    <div class="x_panel">
        <div class="x_content">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Brand</th>
                    <th class="text-center">Logo</th>
                    <th class="text-center">Enable</th>
                    <th class="col-sm-1 text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($card_brands as $key => $card_brand)
                    <tr>
                        <td>{!! (($card_brands->currentPage() - 1) * $card_brands->perPage()) + $key + 1 !!}</td>
                        <td>{!! $card_brand->brand_name !!}</td>
                        <td class="text-center"><img class="img-thumbnail" src="{!! ImageHelper::TinyHeighten($card_brand->logo) !!}"></td>
                        <td class="text-center">
                            @if($card_brand->is_enabled)
                                <i class="fa fa-check-circle fa-lg text-success" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-times-circle fa-lg text-danger" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td class="nowrap">
                            <a href="{!! action('Admin\CardBrandController@edit', $card_brand->uuid) !!}" class="{!! $button['edit'] !!} btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                            {!! Form::open(['class' => 'form-delete', 'url' => action('Admin\CardBrandController@destroy', $card_brand->uuid), 'method' => 'DELETE']) !!}
                            {!! Form::hidden('_method', 'delete') !!}
                            <button type="submit" class="{!! $button['delete'] !!} btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $card_brands->links() !!}
            </div>
        </div>
    </div>
@endsection

@section('styles_before')
    @parent
@endsection

@section('footer_scripts')
    @parent
@endsection

