@extends('admin.layouts.master_admin')

@section('title', 'Cards List')

@section('title-right')
    <div class="pull-right">
        <a href="{!! action('Admin\CardController@create') !!}" class="{!! $button['create'] !!} w-md"><i class="fa fa-plus-square" aria-hidden="true"></i> Create New Card</a>
    </div>
@endsection

@section('content')
    <div class="x_panel">
        <div class="x_content">
            <table class="table table-striped table-hover table-responsive">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Card Name</th>
                    <th>Bank</th>
                    <th>Card Type</th>
                    <th class="text-center">Card Brand</th>
                    <th class="text-center">Image</th>
                    <th class="text-center">Enable</th>
                    <th class="col-sm-1 text-center">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cards as $key => $card)
                    <tr>
                        <td>{!! (($cards->currentPage() - 1) * $cards->perPage()) + $key + 1 !!}</td>
                        <td>{!! $card->card_name !!}</td>
                        <td>{!! $card->bank->bank_nickname !!}</td>
                        <td>{!! $card_types[$card->card_type] !!}</td>
                        <td class="text-center"><img class="img-thumbnail" src="{!! ImageHelper::tinyHeighten($card->brand->logo) !!}"></td>
                        <td class="text-center"><img class="img-thumbnail" src="{!! ImageHelper::tinyHeighten($card->image) !!}"></td>
                        <td class="text-center">
                            @if($card->is_enabled)
                                <i class="fa fa-check-circle fa-lg text-success" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-times-circle fa-lg text-danger" aria-hidden="true"></i>
                            @endif
                        </td>
                        <td class="nowrap">
                            <a href="{!! action('Admin\CardController@edit', $card->uuid) !!}" class="{!! $button['edit'] !!} btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                            {!! Form::open(['class' => 'form-delete', 'url' => action('Admin\CardController@destroy', $card->uuid), 'method' => 'DELETE']) !!}
                            {!! Form::hidden('_method', 'delete') !!}
                            <button type="submit" class="{!! $button['delete'] !!} btn-xs"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {!! $cards->links() !!}
            </div>
        </div>
    </div>
@endsection

@section('styles_before')
    @parent
@endsection

@section('footer_scripts')
    @parent
@endsection

