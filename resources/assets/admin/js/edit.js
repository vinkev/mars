$('.switchery').each(function () {
    new Switchery($(this)[0], {
        color: '#00b19d',
        size: 'small'
    });
});

$(".file-upload").fileinput({
    showUpload: false,
    initialPreviewAsData: true
}).on('fileclear', function (event) {
    $(this).parents('.file-input').siblings('.file-upload-path').val('');
});

$.fn.select2.defaults.set('theme', 'bootstrap');

$('select:not(.select2-custom)').select2({
    'selectOnClose': true
});

$('select.select2-image').select2({
    templateResult: function (state) {
        if (!state.id) {
            return state.text;
        }

        var image_path = $(state.element).data('image');

        if (image_path) {
            return $('<span><img src="' + image_path + '" class="img-select2" /> ' + state.text + '</span>')
        } else {
            return state.text
        }
    }
});

$('input.datepicker').daterangepicker({
    singleDatePicker: true,
    singleClasses: "picker_3",
    autoUpdateInput: true,
    locale: {
        "format": "YYYY-MM-DD",
        "separator": " - ",
        "firstDay": 1
    }
});

$('.wysiwyg').summernote({
    maxHeight: 250,
    minHeight: 250,
    toolbar: [
        // [groupName, [list of button]]
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'hr', 'paragraph']],
        ['height', ['height']],
        ['action', ['undo', 'redo', 'fullscreen']]
    ],
    popover: {
        air: [
            ['color', ['color']],
            ['font', ['bold', 'underline', 'clear']]
        ]
    }
});