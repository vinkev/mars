<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Name of route
    |--------------------------------------------------------------------------
    |
    | Enter the routes name to enable dynamic imagecache manipulation.
    | This handle will define the first part of the URI:
    | 
    | {route}/{template}/{filename}
    | 
    | Examples: "images", "img/cache"
    |
    */

    'route' => 'cdn',

    /*
    |--------------------------------------------------------------------------
    | Storage paths
    |--------------------------------------------------------------------------
    |
    | The following paths will be searched for the image filename, submited 
    | by URI. 
    | 
    | Define as many directories as you like.
    |
    */

    'paths' => array(
        public_path('storage'),
    ),

    /*
    |--------------------------------------------------------------------------
    | Manipulation templates
    |--------------------------------------------------------------------------
    |
    | Here you may specify your own manipulation filter templates.
    | The keys of this array will define which templates 
    | are available in the URI:
    |
    | {route}/{template}/{filename}
    |
    | The values of this array will define which filter class
    | will be applied, by its fully qualified name.
    |
    */

    'templates' => array(
        'tiny-heighten'     => 'Mars\Helpers\Filters\Image\TinyHeighten',
        'small'            => 'Mars\Helpers\Filters\Image\Small',
        'small-square'     => 'Mars\Helpers\Filters\Image\SmallSquare',
        'medium'           => 'Mars\Helpers\Filters\Image\Medium',
        'medium-square'    => 'Mars\Helpers\Filters\Image\MediumSquare',
        'large'            => 'Mars\Helpers\Filters\Image\Large',
        'large-square'     => 'Mars\Helpers\Filters\Image\LargeSquare',
        'thumbnail'        => 'Mars\Helpers\Filters\Image\Thumbnail',
        'medium-thumbnail' => 'Mars\Helpers\Filters\Image\MediumThumbnail',
        'mini-thumbnail'   => 'Mars\Helpers\Filters\Image\MiniThumbnail',
    ),

    /*
    |--------------------------------------------------------------------------
    | Image Cache Lifetime
    |--------------------------------------------------------------------------
    |
    | Lifetime in minutes of the images handled by the imagecache route.
    |
    */

    'lifetime' => 43200,

);
