<?php

return [
    'pagination' => '25',
    'button'     => [
        'create' => 'btn btn-primary',
        'edit'   => 'btn btn-success',
        'delete' => 'btn btn-danger',
        'cancel' => 'btn btn-danger',
        'submit' => 'btn btn-primary',
    ],
    'max_upload' => [ // in KBytes
        'brand'    => [
            'logo' => 1500,
        ],
        'bank'     => [
            'logo' => 1500
        ],
        'card'     => [
            'image' => 1500
        ],
        'merchant' => [
            'logo' => 1500
        ],
        'promo' => [
            'image' => 2500
        ]
    ]
];