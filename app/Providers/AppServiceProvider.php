<?php

namespace Mars\Providers;

use Bouncer;
use DateTime;
use DB;
use Illuminate\Support\ServiceProvider;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Bouncer::cache();
        $this->queryLog();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }

    private function queryLog()
    {
        if (env('SQL_DEBUG', false)) {
            $log     = new Logger('mars-sql');
            $handler = new RotatingFileHandler(storage_path() . '/logs/sql.log', 20, Logger::INFO);
            $handler->setFormatter(new LineFormatter(null, null, true, true));
            $log->pushHandler($handler);

            DB::listen(function ($query) use ($log) {
                $bindings = $query->bindings;
                $sql      = $query->sql;
                $time     = $query->time;

                $data = compact('bindings', 'time');

                // Format binding data for sql insertion
                foreach ($bindings as $i => $binding) {

                    if ($binding instanceof DateTime) {
                        $bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                    } else if (is_string($binding)) {
                        $bindings[$i] = "'$binding'";
                    }
                }

                // Insert bindings into query
                $sql = str_replace(array('%', '?'), array('%%', '%s'), $sql);
                $sql = vsprintf($sql, $bindings);

                $log->addInfo($sql, $data);
            });
        }
    }
}
