<?php

namespace Mars\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Mars\Repositories\Interfaces\UserRepository::class, \Mars\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\Mars\Repositories\Interfaces\UserProfileRepository::class, \Mars\Repositories\UserProfileRepositoryEloquent::class);
        $this->app->bind(\Mars\Repositories\Interfaces\RoleRepository::class, \Mars\Repositories\RoleRepositoryEloquent::class);
        $this->app->bind(\Mars\Repositories\Interfaces\CardBrandRepository::class, \Mars\Repositories\CardBrandRepositoryEloquent::class);
        $this->app->bind(\Mars\Repositories\Interfaces\CardRepository::class, \Mars\Repositories\CardRepositoryEloquent::class);
        $this->app->bind(\Mars\Repositories\Interfaces\BankRepository::class, \Mars\Repositories\BankRepositoryEloquent::class);
        $this->app->bind(\Mars\Repositories\Interfaces\AddressRepository::class, \Mars\Repositories\AddressRepositoryEloquent::class);
        $this->app->bind(\Mars\Repositories\Interfaces\PromoRepository::class, \Mars\Repositories\PromoRepositoryEloquent::class);
        $this->app->bind(\Mars\Repositories\Interfaces\MerchantRepository::class, \Mars\Repositories\MerchantRepositoryEloquent::class);
        //:end-bindings:
    }
}
