<?php

namespace Mars\Providers;

use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Mars\Models\Bank;
use Mars\Models\Card;
use Mars\Models\Merchant;

class PolymorphismServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'banks' => Bank::class,
            'merchants' => Merchant::class,
            'cards' => Card::class
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
