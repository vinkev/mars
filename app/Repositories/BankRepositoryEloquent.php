<?php

namespace Mars\Repositories;

use DB;
use Exception;
use Illuminate\Container\Container as Application;
use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Models\Bank;
use Mars\Repositories\Interfaces\AddressRepository;
use Mars\Repositories\Interfaces\bankRepository;
use Mars\Validators\BankValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityDeleted;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class BankRepositoryEloquent
 *
 * @package namespace Mars\Repositories;
 */
class BankRepositoryEloquent extends BaseRepository implements BankRepository
{
    use CacheableRepository;

    /**
     * @var AddressRepository
     */
    private $address_repository;

    /**
     * BankRepositoryEloquent constructor.
     *
     * @param Application $app
     * @param AddressRepository $address_repository
     */
    public function __construct(Application $app, AddressRepository $address_repository)
    {
        parent::__construct($app);
        $this->address_repository = $address_repository;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Bank::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return BankValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $postdata
     * @return \Illuminate\Database\Eloquent\Builder|Bank
     * @throws FileUploadException
     * @throws ModelException
     */
    public function createBank($postdata)
    {
        $logo_path = $this->storeLogo($postdata);

        try {
            $bank = DB::transaction(function () use ($postdata, $logo_path) {
                $bank    = $this->create($this->mappingInput($postdata, $logo_path));
                $address = $this->address_repository->createAddress($postdata, $bank);

                return $bank;
            }, 5);

        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on creating bank');
        }

        return $bank;
    }

    /**
     * @param array $postdata
     * @param \Mars\Models\Bank $bank
     * @return bool
     * @throws FileUploadException
     * @throws ModelException
     */
    public function updateBank($postdata, $bank)
    {
        $logo_path = $this->storeLogo($postdata);

        try {
            $result = DB::transaction(function () use ($postdata, $logo_path, $bank) {
                $result_bank    = $bank->update($this->mappingInput($postdata, $logo_path));
                $result_address = $this->address_repository->updateAddress($postdata, $bank, $bank->address);

                return $result_bank && $result_address;
            }, 5);
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating bank');
        }

        event(new RepositoryEntityUpdated($this, $bank));

        return $result;
    }

    /**
     * @param \Mars\Models\Bank $bank
     * @return bool
     * @throws ModelException
     */
    public function deleteBank($bank)
    {
        try {
            $result = $bank->delete();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on deleting bank');
        }

        event(new RepositoryEntityDeleted($this, $bank));

        return $result;
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank
     */
    public function getBankById($id)
    {
        return $this->findByField('id', $id);
    }

    /**
     * @param string $uuid
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|Bank
     */
    public function getBankByUuid($uuid, $full_join = true)
    {
        if ($full_join) {
            return $this->with('address')->findByField('uuid', $uuid);
        } else {
            return $this->findByField('uuid', $uuid);
        }
    }

    /**
     * @param array $postdata
     * @return string|null $logo_path
     * @throws FileUploadException
     */
    private function storeLogo($postdata)
    {
        if (!empty($postdata['logo'])) {
            try {
                $logo_path = $postdata['logo']->store('images/banks');
            } catch (Exception $e) {
                throw new FileUploadException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on uploading bank logo');
            }
        } else {
            $logo_path = null;
        }

        return $logo_path;
    }

    private function mappingInput($postdata, $logo_path)
    {
        $map = [
            'is_enabled'         => !empty($postdata['is_enabled']) ? $postdata['is_enabled'] : 0,
            'bank_name'          => $postdata['bank_name'],
            'bank_nickname'      => $postdata['bank_nickname'],
            'bank_type'          => $postdata['bank_type'],
            'transfer_bank_code' => $postdata['transfer_bank_code'],
            'website'            => $postdata['website'],
            'description'        => $postdata['description'],
        ];

        if (!empty($logo_path)) {
            $map['logo'] = $logo_path;
        }

        return $map;
    }
}
