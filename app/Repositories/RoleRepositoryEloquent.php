<?php

namespace Mars\Repositories;

use Exception;
use Mars\Exceptions\ModelException;
use Mars\Models\Role;
use Mars\Repositories\Interfaces\RoleRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityDeleted;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class RoleRepositoryEloquent
 *
 * @package namespace Mars\Repositories;
 */
class RoleRepositoryEloquent extends BaseRepository implements RoleRepository
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $postdata
     * @param int $user_id
     * @return \Illuminate\Database\Eloquent\Builder|Role
     * @throws ModelException
     */
    public function createRole($postdata, $user_id)
    {
        try {
            $role = $this->create($this->mappingInput($postdata));
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on creating role');
        }

        event(new RepositoryEntityCreated($this, $role));

        return $role;
    }

    /**
     * @param array $postdata
     * @param \Mars\Models\Role $role
     * @param int $user_id
     * @return bool
     * @throws ModelException
     */
    public function updateRole($postdata, $role, $user_id)
    {
        try {
            $result = $role->update($this->mappingInput($postdata));
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating role');
        }

        event(new RepositoryEntityUpdated($this, $role));

        return $result;
    }

    /**
     * @param \Mars\Models\Role $role
     * @return bool
     * @throws ModelException
     */
    public function deleteRole($role)
    {
        try {
            $result = $role->delete();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on deleting role');
        }

        event(new RepositoryEntityDeleted($this, $role));

        return $result;
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role
     */
    public function getRoleById($id)
    {
        return $this->findByField('id', $id);
    }

    private function mappingInput($postdata)
    {
        return [
            'name'  => $postdata['code'],
            'title' => $postdata['title']
        ];
    }

}
