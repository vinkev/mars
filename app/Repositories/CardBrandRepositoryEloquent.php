<?php

namespace Mars\Repositories;

use Exception;
use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Models\CardBrand;
use Mars\Repositories\Interfaces\CardBrandRepository;
use Mars\Validators\CardBrandValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityDeleted;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class CardBrandRepositoryEloquent
 *
 * @package namespace Mars\Repositories;
 */
class CardBrandRepositoryEloquent extends BaseRepository implements CardBrandRepository
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CardBrand::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return CardBrandValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $postdata
     * @return \Illuminate\Database\Eloquent\Builder|CardBrand
     * @throws FileUploadException
     * @throws ModelException
     */
    public function createCardBrand($postdata)
    {
        $logo_path = $this->storeLogo($postdata);

        try {
            $card_brand = $this->create($this->mappingInput($postdata, $logo_path));
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on creating card brand');
        }

        event(new RepositoryEntityCreated($this, $card_brand));

        return $card_brand;
    }

    /**
     * @param array $postdata
     * @param \Mars\Models\CardBrand $card_brand
     * @return bool
     * @throws FileUploadException
     * @throws ModelException
     */
    public function updateCardBrand($postdata, $card_brand)
    {
        $logo_path = $this->storeLogo($postdata);

        try {
            $result = $card_brand->update($this->mappingInput($postdata, $logo_path));
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating card brand');
        }

        event(new RepositoryEntityUpdated($this, $card_brand));

        return $result;
    }

    /**
     * @param \Mars\Models\CardBrand $card_brand
     * @return bool
     * @throws ModelException
     */
    public function deleteCardBrand($card_brand)
    {
        try {
            $result = $card_brand->delete();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on deleting card brand');
        }

        event(new RepositoryEntityDeleted($this, $card_brand));

        return $result;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand
     */
    public function getCardBrandById($id)
    {
        return $this->findByField('id', $id);
    }

    /**
     * @param string $uuid
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand
     */
    public function getCardBrandByUuid($uuid)
    {
        return $this->findByField('uuid', $uuid);
    }

    /**
     * @param array $postdata
     * @return string|null $logo_path
     * @throws FileUploadException
     */
    private function storeLogo($postdata)
    {
        if (!empty($postdata['logo'])) {
            try {
                $logo_path = $postdata['logo']->store('images/brands');
            } catch (Exception $e) {
                throw new FileUploadException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on uploading card brand logo');
            }
        } else {
            $logo_path = null;
        }

        return $logo_path;
    }

    private function mappingInput($postdata, $logo_path = null)
    {
        $map = [
            'is_enabled'  => !empty($postdata['is_enabled']) ? $postdata['is_enabled'] : 0,
            'brand_name'  => $postdata['brand_name'],
            'description' => $postdata['description'],
        ];

        if (!empty($logo_path)) {
            $map['logo'] = $logo_path;
        }

        return $map;
    }
}
