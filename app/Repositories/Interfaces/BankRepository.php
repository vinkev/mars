<?php

namespace Mars\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BankRepository
 * @package namespace Mars\Repositories\Interfaces;
 */
interface BankRepository extends RepositoryInterface
{
    /**
     * @param array $postdata
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank
     */
    public function createBank($postdata);

    /**
     * @param array $postdata
     * @param \Mars\Models\Bank $bank
     * @return bool
     */
    public function updateBank($postdata, $bank);

    /**
     * @param \Mars\Models\Bank $bank
     * @return bool
     */
    public function deleteBank($bank);

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank
     */
    public function getBankById($id);

    /**
     * @param string $uuid
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank
     */
    public function getBankByUuid($uuid, $full_join = true);
}
