<?php

namespace Mars\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace Mars\Repositories;
 */
interface UserRepository extends RepositoryInterface
{

    /**
     * @param int $user_id
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\User
     */
    public function getUserById($user_id, $full_join = true);
}
