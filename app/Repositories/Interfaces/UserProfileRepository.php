<?php

namespace Mars\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserProfileRepository
 * @package namespace Mars\Repositories\Interfaces;
 */
interface UserProfileRepository extends RepositoryInterface
{
}
