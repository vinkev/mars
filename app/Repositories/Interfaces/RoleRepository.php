<?php

namespace Mars\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RoleRepository
 * @package namespace Mars\Repositories\Interfaces;
 */
interface RoleRepository extends RepositoryInterface
{
    /**
     * @param array $postdata
     * @param int $user_id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role
     */
    public function createRole($postdata, $user_id);

    /**
     * @param array $postdata
     * @param \Mars\Models\Role $role
     * @param int $user_id
     * @return bool
     */
    public function updateRole($postdata, $role, $user_id);

    /**
     * @param \Mars\Models\Role $role
     * @return bool
     */
    public function deleteRole($role);

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role
     */
    public function getRoleById($id);
}

