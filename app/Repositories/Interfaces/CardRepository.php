<?php

namespace Mars\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CardRepository
 *
 * @package namespace Mars\Repositories\Interfaces;
 */
interface CardRepository extends RepositoryInterface
{
    /**
     * @param array $postdata
     * @param int $bank_id
     * @param int $card_brand_id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card
     */
    public function createCard($postdata, $bank_id, $card_brand_id);

    /**
     * @param array $postdata
     * @param int $bank_id
     * @param int $card_brand_id
     * @param \Mars\Models\Card $card
     * @return bool
     */
    public function updateCard($postdata, $bank_id, $card_brand_id, $card);

    /**
     * @param \Mars\Models\Card $card
     * @return bool
     */
    public function deleteCard($card);

    /**
     * @param string $uuid
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card
     */
    public function getCardByUuid($uuid, $full_join = true);

    /**
     * @param array $uuids
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card
     */
    public function getCardsByUuids($uuids);
}
