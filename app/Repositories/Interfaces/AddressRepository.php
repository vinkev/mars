<?php

namespace Mars\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AddressRepository
 *
 * @package namespace Mars\Repositories\Interfaces;
 */
interface AddressRepository extends RepositoryInterface
{
    /**
     * @param array $postdata
     * @param \Mars\Models\Bank|\Mars\Models\Merchant $polymorphic_model
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address
     * @internal param $model
     */
    public function createAddress($postdata, $polymorphic_model);

    /**
     * @param array $postdata
     * @param \Mars\Models\Bank|\Mars\Models\Merchant $polymorphic_model
     * @param \Mars\Models\Address $address
     * @return bool
     */
    public function updateAddress($postdata, $polymorphic_model, $address);

    /**
     * @param \Mars\Models\Bank|\Mars\Models\Merchant $polymorphic_model
     * @param \Mars\Models\Address $address
     * @return bool
     */
    public function deleteAddress($polymorphic_model, $address);

    /**
     * @param string $uuid
     * @param int $polymorphic_id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address
     */
    public function getAddressByUuidAndPolymorphicId($uuid, $polymorphic_id);
}
