<?php

namespace Mars\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PromoRepository
 * @package namespace Mars\Repositories\Interfaces;
 */
interface PromoRepository extends RepositoryInterface
{
    /**
     * @param array $postdata
     * @param int $merchant_id
     * @param array $card_ids
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo
     */
    public function createPromo($postdata, $merchant_id, $card_ids);

    /**
     * @param array $postdata
     * @param int $merchant_id
     * @param array $card_ids
     * @param \Mars\Models\Promo $promo
     * @return bool
     */
    public function updatePromo($postdata, $merchant_id, $card_ids, $promo);

    /**
     * @param \Mars\Models\Promo $promo
     * @return bool
     */
    public function deletePromo($promo);

    /**
     * @param string $uuid
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo
     */
    public function getPromoByUuid($uuid, $full_join = true);
}
