<?php

namespace Mars\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CardBrandRepository
 *
 * @package namespace Mars\Repositories\Interfaces;
 */
interface CardBrandRepository extends RepositoryInterface
{
    /**
     * @param array $postdata
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand
     */
    public function createCardBrand($postdata);

    /**
     * @param array $postdata
     * @param \Mars\Models\CardBrand $card_brand
     * @return bool
     */
    public function updateCardBrand($postdata, $card_brand);

    /**
     * @param \Mars\Models\CardBrand $card_brand
     * @return bool
     */
    public function deleteCardBrand($card_brand);

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand
     */
    public function getCardBrandById($id);

    /**
     * @param string $uuid
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand
     */
    public function getCardBrandByUuid($uuid);
}
