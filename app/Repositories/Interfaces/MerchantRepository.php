<?php

namespace Mars\Repositories\Interfaces;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MerchantRepository
 *
 * @package namespace Mars\Repositories\Interfaces;
 */
interface MerchantRepository extends RepositoryInterface
{
    /**
     * @param array $postdata
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant
     */
    public function createMerchant($postdata);

    /**
     * @param array $postdata
     * @param \Mars\Models\Merchant $merchant
     * @return bool
     */
    public function updateMerchant($postdata, $merchant);

    /**
     * @param \Mars\Models\Merchant $merchant
     * @return bool
     */
    public function deleteMerchant($merchant);

    /**
     * @param string $uuid
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant
     */
    public function getMerchantByUuid($uuid, $full_join = true);
}
