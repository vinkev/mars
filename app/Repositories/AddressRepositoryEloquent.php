<?php

namespace Mars\Repositories;

use Exception;
use Mars\Exceptions\ModelException;
use Mars\Models\Address;
use Mars\Models\Bank;
use Mars\Repositories\Interfaces\AddressRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityDeleted;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class AddressRepositoryEloquent
 *
 * @package namespace Mars\Repositories;
 */
class AddressRepositoryEloquent extends BaseRepository implements AddressRepository
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Address::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $postdata
     * @param Bank $polymorphic_model
     * @return mixed
     * @throws ModelException
     */
    public function createAddress($postdata, $polymorphic_model)
    {
        $address = $this->makeModel();
        $address = $this->mappingInput($postdata, $address);
        try {
            if (method_exists($polymorphic_model, 'address')) {
                $polymorphic_model->address()->save($address);
            } elseif (method_exists($polymorphic_model, 'addresses')) {
                $polymorphic_model->addresses()->save($address);
            }
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on creating address');
        }

        event(new RepositoryEntityCreated($this, $address));
        event(new RepositoryEntityCreated($polymorphic_model->repositoryInterface(), $polymorphic_model));

        return $address;
    }

    /**
     * @param array $postdata
     * @param \Mars\Models\Bank|\Mars\Models\Merchant $polymorphic_model
     * @param \Mars\Models\Address $address
     * @return bool
     * @throws ModelException
     */
    public function updateAddress($postdata, $polymorphic_model, $address)
    {
        try {
            $address = $this->mappingInput($postdata, $address);
            $result  = $address->save();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating address');
        }

        event(new RepositoryEntityUpdated($this, $address));
        event(new RepositoryEntityUpdated($polymorphic_model->repositoryInterface(), $polymorphic_model));

        return $result;
    }

    /**
     * @param string $uuid
     * @param int $polymorphic_id
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address
     */
    public function getAddressByUuidAndPolymorphicId($uuid, $polymorphic_id)
    {
        return $this->findWhere([
            'uuid'           => $uuid,
            'addressable_id' => $polymorphic_id
        ]);
    }

    /**
     * @param \Mars\Models\Bank|\Mars\Models\Merchant $polymorphic_model
     * @param \Mars\Models\Address $address
     * @return bool
     * @throws ModelException
     */
    public function deleteAddress($polymorphic_model, $address)
    {
        try {
            $result = $address->delete();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on deleting address');
        }

        event(new RepositoryEntityDeleted($this, $address));
        event(new RepositoryEntityDeleted($polymorphic_model->repositoryInterface(), $polymorphic_model));

        return $result;
    }

    /**
     * @param array $postdata
     * @param Address $address
     * @return Address
     */
    private function mappingInput($postdata, $address)
    {
        $address->is_enabled    = array_key_exists('is_enabled', $postdata) ? $postdata['is_enabled'] : 1;
        $address->location_name = trim($postdata['location_name']);
        $address->location_info = trim($postdata['location_info']);
        $address->address       = trim($postdata['address']);
        $address->city          = trim($postdata['city']);
        $address->state         = trim($postdata['state']);
        $address->country       = 'ID';
        $address->zip_code      = $postdata['zip_code'];

        $office_phone = [];
        if (is_array($postdata['office_phone'])) {
            foreach ($postdata['office_phone'] as $phone) {
                if (!empty($phone)) {
                    $office_phone[] = $phone;
                }
            }
        }
        $address->office_phone = $office_phone;

        $fax_phone = [];
        if (is_array($postdata['fax_phone'])) {
            foreach ($postdata['fax_phone'] as $phone) {
                if (!empty($phone)) {
                    $fax_phone[] = $phone;
                }
            }
        }
        $address->fax_phone = $fax_phone;

        return $address;
    }
}
