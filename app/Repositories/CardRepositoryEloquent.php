<?php

namespace Mars\Repositories;

use Exception;
use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Models\Card;
use Mars\Repositories\Interfaces\CardRepository;
use Mars\Validators\CardValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityDeleted;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class CardRepositoryEloquent
 *
 * @package namespace Mars\Repositories;
 */
class CardRepositoryEloquent extends BaseRepository implements CardRepository
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Card::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return CardValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
//        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $postdata
     * @param int $bank_id
     * @param int $card_brand_id
     * @return \Illuminate\Database\Eloquent\Builder|Card
     * @throws ModelException
     */
    public function createCard($postdata, $bank_id, $card_brand_id)
    {
        $image_path = $this->storeImage($postdata);

        try {
            $card = $this->create($this->mappingInput($postdata, $bank_id, $card_brand_id, $image_path));
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on creating card');
        }

        return $card;
    }

    /**
     * @param array $postdata
     * @param int $bank_id
     * @param int $card_brand_id
     * @param \Mars\Models\Card $card
     * @return bool
     * @throws ModelException
     */
    public function updateCard($postdata, $bank_id, $card_brand_id, $card)
    {
        $image_path = $this->storeImage($postdata);

        try {
            $result = $card->update($this->mappingInput($postdata, $bank_id, $card_brand_id, $image_path));
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating card');
        }

        event(new RepositoryEntityUpdated($this, $card));

        return $result;
    }

    /**
     * @param \Mars\Models\Card $card
     * @return bool
     * @throws ModelException
     */
    public function deleteCard($card)
    {
        try {
            $result = $card->delete();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on deleting card');
        }

        event(new RepositoryEntityDeleted($this, $card));

        return $result;
    }

    /**
     * @param string $uuid
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|Card
     */
    public function getCardByUuid($uuid, $full_join = true)
    {
        if ($full_join) {
            return $this->with(['bank', 'brand'])->findByField('uuid', $uuid);
        } else {
            return $this->findByField('uuid', $uuid);
        }
    }

    /**
     * @param array $uuids
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card
     */
    public function getCardsByUuids($uuids)
    {
        return $this->findWhereIn('uuid', $uuids);
    }

    /**
     * @param array $postdata
     * @return string|null $image_path
     * @throws FileUploadException
     */
    private function storeImage($postdata)
    {
        if (!empty($postdata['image'])) {
            try {
                $image_path = $postdata['image']->store('images/cards');
            } catch (Exception $e) {
                throw new FileUploadException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on uploading card image');
            }
        } else {
            $image_path = null;
        }

        return $image_path;
    }

    private function mappingInput($postdata, $bank_id, $card_brand_id, $image_path = null)
    {
        $map = [
            'is_enabled'    => !empty($postdata['is_enabled']) ? $postdata['is_enabled'] : 0,
            'bank_id'       => $bank_id,
            'card_brand_id' => $card_brand_id,
            'card_name'     => $postdata['card_name'],
            'card_type'     => $postdata['card_type']
        ];

        if (!empty($image_path)) {
            $map['image'] = $image_path;
        }

        return $map;
    }
}
