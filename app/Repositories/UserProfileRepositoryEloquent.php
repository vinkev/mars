<?php

namespace Mars\Repositories;

use Mars\Models\UserProfile;
use Mars\Repositories\Interfaces\UserProfileRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class UserProfileRepositoryEloquent
 *
 * @package namespace Mars\Repositories;
 */
class UserProfileRepositoryEloquent extends BaseRepository implements UserProfileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserProfile::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
