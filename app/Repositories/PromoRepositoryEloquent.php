<?php

namespace Mars\Repositories;

use Carbon\Carbon;
use DB;
use Exception;
use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Models\Promo;
use Mars\Repositories\Interfaces\PromoRepository;
use Mars\Validators\PromoValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityDeleted;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class PromoRepositoryEloquent
 *
 * @package namespace Mars\Repositories;
 */
class PromoRepositoryEloquent extends BaseRepository implements PromoRepository
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Promo::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return PromoValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $postdata
     * @param int $merchant_id
     * @param array $card_ids
     * @return \Illuminate\Database\Eloquent\Builder|Promo
     * @throws ModelException
     */
    public function createPromo($postdata, $merchant_id, $card_ids)
    {
        $image_path = $this->storeImage($postdata);

        try {
            $promo = DB::transaction(function () use ($postdata, $merchant_id, $card_ids, $image_path) {
                $promo = $this->create($this->mappingInput($postdata, $merchant_id, $image_path));
                $promo->cards()->sync($card_ids);
            }, 5);
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on creating promo');
        }

        return $promo;
    }

    /**
     * @param array $postdata
     * @param int $merchant_id
     * @param array $card_ids
     * @param \Mars\Models\Promo $promo
     * @return bool
     * @throws ModelException
     */
    public function updatePromo($postdata, $merchant_id, $card_ids, $promo)
    {
        $image_path = $this->storeImage($postdata);

        try {
            $result = DB::transaction(function () use ($postdata, $merchant_id, $card_ids, $image_path, $promo) {
                $result = $promo->update($this->mappingInput($postdata, $merchant_id, $image_path));
                $promo->cards()->sync($card_ids);

                return $result;
            }, 5);
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating promo');
        }

        event(new RepositoryEntityUpdated($this, $promo));

        return $result;
    }

    /**
     * @param \Mars\Models\Promo $promo
     * @return bool
     * @throws ModelException
     */
    public function deletePromo($promo)
    {
        try {
            $result = $promo->delete();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on deleting promo');
        }

        event(new RepositoryEntityDeleted($this, $promo));

        return $result;
    }

    /**
     * @param string $uuid
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo
     */
    public function getPromoByUuid($uuid, $full_join = true)
    {
        if ($full_join) {
            return $this->with(['cards', 'merchant'])->findByField('uuid', $uuid);
        } else {
            return $this->findByField('uuid', $uuid);
        }
    }

    /**
     * @param array $postdata
     * @return string|null $image_path
     * @throws FileUploadException
     */
    private function storeImage($postdata)
    {
        if (!empty($postdata['image'])) {
            try {
                $image_path = $postdata['image']->store('images/promos');
            } catch (Exception $e) {
                throw new FileUploadException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on uploading promo image');
            }
        } else {
            $image_path = null;
        }

        return $image_path;
    }

    private function mappingInput($postdata, $merchant_id, $image_path)
    {
        $map = [
            'is_enabled'     => !empty($postdata['is_enabled']) ? $postdata['is_enabled'] : 0,
            'promo_title'    => $postdata['promo_title'],
            'promo_subtitle' => $postdata['promo_subtitle'],
            'merchant_id'    => $merchant_id,
            'publish_at'     => Carbon::parse($postdata['publish_at'])->toDateTimeString(),
            'start_at'       => Carbon::parse($postdata['start_at'])->toDateTimeString(),
            'end_at'         => Carbon::parse($postdata['end_at'])->toDateTimeString(),
            'description'    => $postdata['description'],
            'term_condition' => $postdata['term_condition'],
        ];

        if (!empty($image_path)) {
            $map['image'] = $image_path;
        }

        return $map;
    }
}
