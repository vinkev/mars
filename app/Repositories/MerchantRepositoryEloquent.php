<?php

namespace Mars\Repositories;

use Exception;
use Illuminate\Container\Container as Application;
use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Models\Merchant;
use Mars\Repositories\Interfaces\AddressRepository;
use Mars\Repositories\Interfaces\MerchantRepository;
use Mars\Validators\MerchantValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Events\RepositoryEntityDeleted;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class MerchantRepositoryEloquent
 *
 * @package namespace Mars\Repositories;
 */
class MerchantRepositoryEloquent extends BaseRepository implements MerchantRepository
{
    use CacheableRepository;

    /**
     * @var AddressRepository
     */
    private $address_repository;

    /**
     * MerchantRepositoryEloquent constructor.
     *
     * @param Application $app
     * @param AddressRepository $address_repository
     */
    public function __construct(Application $app, AddressRepository $address_repository)
    {
        parent::__construct($app);
        $this->address_repository = $address_repository;
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Merchant::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return MerchantValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
//        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param array $postdata
     * @return \Illuminate\Database\Eloquent\Builder|Merchant
     * @throws ModelException
     */
    public function createMerchant($postdata)
    {
        $logo_path = $this->storeLogo($postdata);

        try {
            $merchant = $this->create($this->mappingInput($postdata, $logo_path));

        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on creating merchant');
        }

        return $merchant;
    }

    /**
     * @param array $postdata
     * @param \Mars\Models\Merchant $merchant
     * @return bool
     * @throws ModelException
     */
    public function updateMerchant($postdata, $merchant)
    {
        $logo_path = $this->storeLogo($postdata);

        try {
            $result = $merchant->update($this->mappingInput($postdata, $logo_path));

        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on updating merchant');
        }

        event(new RepositoryEntityUpdated($this, $merchant));

        return $result;
    }

    /**
     * @param \Mars\Models\Merchant $merchant
     * @return bool
     * @throws ModelException
     */
    public function deleteMerchant($merchant)
    {
        try {
            $result = $merchant->delete();
        } catch (Exception $e) {
            throw new ModelException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on deleting merchant');
        }

        event(new RepositoryEntityDeleted($this, $merchant));

        return $result;
    }

    /**
     * @param string $uuid
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant
     */
    public function getMerchantByUuid($uuid, $full_join = true)
    {
        if ($full_join) {
            return $this->with(['addresses' => function($query){
                return $query->orderBy('location_name', 'ASC');
            }])->findByField('uuid', $uuid);
        } else {
            return $this->findByField('uuid', $uuid);
        }
    }


    /**
     * @param array $postdata
     * @return string|null $logo_path
     * @throws FileUploadException
     */
    private function storeLogo($postdata)
    {
        if (!empty($postdata['logo'])) {
            try {
                $logo_path = $postdata['logo']->store('images/merchants');
            } catch (Exception $e) {
                throw new FileUploadException($e->getMessage(), $e->getCode(), $e->getPrevious(), 'Error on uploading merchant logo');
            }
        } else {
            $logo_path = null;
        }

        return $logo_path;
    }

    private function mappingInput($postdata, $logo_path)
    {
        $map = [
            'is_enabled'    => !empty($postdata['is_enabled']) ? $postdata['is_enabled'] : 0,
            'merchant_name' => $postdata['merchant_name'],
            'website'       => $postdata['website'],
            'description'   => $postdata['description'],
        ];

        if (!empty($logo_path)) {
            $map['logo'] = $logo_path;
        }

        return $map;
    }
}
