<?php

namespace Mars\Repositories;

use Mars\Models\User;
use Mars\Repositories\Interfaces\UserRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class UserRepositoryEloquent
 *
 * @package namespace Mars\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    use CacheableRepository;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
//        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param int $user_id
     * @param bool $full_join
     * @return \Illuminate\Database\Eloquent\Builder|\Mars\Models\User
     */
    public function getUserById($user_id, $full_join = true)
    {
        if($full_join) {
            return $this->with('profile')->findByField('id', $user_id);
        } else {
            return $this->findByField('id', $user_id);
        }
    }
}
