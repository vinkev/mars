<?php

namespace Mars\Presenters;

use Mars\Transformers\MerchantTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MerchantPresenter
 *
 * @package namespace Mars\Presenters;
 */
class MerchantPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MerchantTransformer();
    }
}
