<?php

namespace Mars\Presenters;

use Mars\Transformers\CardTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CardPresenter
 *
 * @package namespace Mars\Presenters;
 */
class CardPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CardTransformer();
    }
}
