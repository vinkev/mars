<?php

namespace Mars\Presenters;

use Mars\Transformers\CardBrandTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CardBrandPresenter
 *
 * @package namespace Mars\Presenters;
 */
class CardBrandPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CardBrandTransformer();
    }
}
