<?php

namespace Mars\Presenters;

use Mars\Transformers\PromoTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PromoPresenter
 *
 * @package namespace Mars\Presenters;
 */
class PromoPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PromoTransformer();
    }
}
