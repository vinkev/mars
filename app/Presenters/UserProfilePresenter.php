<?php

namespace Mars\Presenters;

use Mars\Transformers\UserProfileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class UserProfilePresenter
 *
 * @package namespace Mars\Presenters;
 */
class UserProfilePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UserProfileTransformer();
    }
}
