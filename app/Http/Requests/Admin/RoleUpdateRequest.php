<?php

namespace Mars\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Mars\Helpers\Constants\DBTable;

class RoleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|string|max:255|unique:' . DBTable::ROLES . ',name,' . request()->route('role'),
            'title' => 'required|string|max:255'
        ];
    }
}
