<?php

namespace Mars\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Mars\Helpers\Constants\CardType;
use Mars\Helpers\Constants\DBTable;

class CardStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_enabled' => 'boolean',
            'bank'       => 'required|exists:' . DBTable::BANKS . ',uuid',
            'card_brand' => 'required|exists:' . DBTable::CARD_BRANDS . ',uuid',
            'card_name'  => 'required|string|max:255|unique:' . DBTable::CARDS . ',card_name',
            'card_type'  => 'required|in:' . implode(',', CardType::getConstants()),
            'image'      => 'required|image|max:' . config('domain.general.max_upload.card.image'),
        ];
    }
}
