<?php

namespace Mars\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Mars\Helpers\Constants\DBTable;

class CardBrandUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_enabled'  => 'boolean',
            'brand_name'  => 'required|string|max:255|unique:' . DBTable::CARD_BRANDS . ',brand_name,' . request()->route('card_brand') . ',uuid',
            'logo'        => 'required_without:logo_path|image|max:' . config('domain.general.max_upload.brand.logo'),
            'description' => 'present|max:800'
        ];
    }

    public function messages()
    {
        $messages                          = parent::messages();
        $messages['logo.required_without'] = 'The :attribute field is required.';

        return $messages;
    }
}
