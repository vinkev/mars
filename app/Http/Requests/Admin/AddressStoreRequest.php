<?php

namespace Mars\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AddressStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_enabled'     => 'sometimes|boolean',
            'location_name'  => 'nullable|string|max:255',
            'location_info'  => 'nullable|string|max:255',
            'address'        => 'required|string|max:255',
            'city'           => 'required|string|max:255',
            'state'          => 'required|string|max:255',
            'zip_code'       => 'required|numeric|digits:5',
            'office_phone.*' => 'nullable|phone:AUTO,ID',
            'fax_phone.*'    => 'nullable|phone:AUTO,ID',
        ];
    }
}
