<?php

namespace Mars\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PromoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_enabled'       => 'sometimes|boolean',
            'promo_title'      => 'required|string|max:255',
            'promo_subtitle'   => 'nullable|string|max:255',
            'payment_method'   => 'array',
            'payment_method.*' => 'required|string|max:32',
            'merchant'         => 'required|string|max:32',
            'publish_at'       => 'required|date',
            'start_at'         => 'required|date',
            'end_at'           => 'required|date',
            'image'            => 'required|image|max:' . config('domain.general.max_upload.promo.image'),
            'description'      => 'nullable|string|max:2000',
            'term_condition'   => 'nullable|string|max:2000',
        ];
    }
}
