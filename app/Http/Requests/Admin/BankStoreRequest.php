<?php

namespace Mars\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Mars\Helpers\Constants\BankType;
use Mars\Helpers\Constants\DBTable;

class BankStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'is_enabled'         => 'sometimes|boolean',
            'bank_name'          => 'required|string|max:255|unique:' . DBTable::BANKS . ',bank_name',
            'bank_nickname'      => 'sometimes|max:255|unique:' . DBTable::BANKS . ',bank_nickname',
            'bank_type'          => 'required|in:' . implode(',', BankType::getConstants()),
            'transfer_bank_code' => 'required|numeric|digits:3',
            'logo'               => 'required|image|max:' . config('domain.general.max_upload.bank.logo'),
            'website'            => 'nullable|url|max:255',
            'description'        => 'nullable|string|max:800',
            'location_name'      => 'nullable|string|max:255',
            'location_info'      => 'nullable|string|max:255',
            'address'            => 'required|string|max:255',
            'city'               => 'required|string|max:255',
            'zip_code'           => 'required|numeric|digits:5',
            'office_phone.*'     => 'nullable|phone:AUTO,ID',
            'fax_phone.*'        => 'nullable|phone:AUTO,ID',
        ];
    }

    public function messages()
    {
        $messages                   = parent::messages();
        $messages['office_phone.*'] = 'The office phone format is invalid';
        $messages['fax_phone.*']    = 'The fax phone format is invalid';

        return $messages;
    }
}
