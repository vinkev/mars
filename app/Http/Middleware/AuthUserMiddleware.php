<?php

namespace Mars\Http\Middleware;

use Closure;
use Mars\Repositories\UserRepositoryEloquent;
use View;

class AuthUserMiddleware
{
    protected $user_repository;

    public function __construct(UserRepositoryEloquent $user_repository_eloquent){
        $this->user_repository = $user_repository_eloquent;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth()->check()) {
            $user = $this->user_repository->getUserById(Auth()->id())->first();
            view()->share('self', $user);
        }
        return $next($request);
    }
}
