<?php

namespace Mars\Http\Controllers\Admin;

use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Http\Requests\Admin\CardStoreRequest;
use Mars\Http\Requests\Admin\CardUpdateRequest;
use Mars\Repositories\Interfaces\BankRepository;
use Mars\Repositories\Interfaces\CardBrandRepository;
use Mars\Repositories\Interfaces\CardRepository;

class CardController extends BaseController
{
    /**
     * @var CardRepository
     */
    private $card_repository;
    /**
     * @var BankRepository
     */
    private $bank_repository;
    /**
     * @var CardBrandRepository
     */
    private $card_brand_repository;

    /**
     * CardController constructor.
     *
     * @param CardRepository $card_repository
     * @param BankRepository $bank_repository
     * @param CardBrandRepository $card_brand_repository
     */
    public function __construct(CardRepository $card_repository, BankRepository $bank_repository, CardBrandRepository $card_brand_repository)
    {
        parent::__construct();
        $this->card_repository       = $card_repository;
        $this->bank_repository       = $bank_repository;
        $this->card_brand_repository = $card_brand_repository;

        if(request()->isMethod('GET')) {
            app('view')->share('card_types', __('admin/lookup.card.type'));
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = $this->card_repository->with([
            'bank' => function ($query) {
                $query->orderBy('bank_nickname', 'ASC');
            }, 'brand'
        ])->orderBy('card_name', 'ASC')->paginate(config('domain.general.pagination'));

        return view('admin.cards.index', compact('cards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $card        = $this->card_repository->makeModel();
        $card->bank  = $this->bank_repository->makeModel();
        $card->brand = $this->card_brand_repository->makeModel();

        $banks       = $this->bank_repository
            ->orderBy('bank_nickname', 'ASC')->all(['uuid', 'bank_nickname'])->pluck('bank_nickname', 'uuid');
        $card_brands = $this->card_brand_repository
            ->orderBy('brand_name', 'ASC')->all(['id', 'uuid', 'brand_name', 'logo']);

        return view('admin.cards.create', compact('card', 'banks', 'card_brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request|CardStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CardStoreRequest $request)
    {
        $postdata = $request->all();

        $bank = $this->bank_repository->getBankByUuid($postdata['bank'], false)->first();
        if (empty($bank)) {
            return abort(404);
        }

        $card_brand = $this->card_brand_repository->getCardBrandByUuid($postdata['card_brand'])->first();
        if (empty($card_brand)) {
            return abort(404);
        }

        try {
            $this->card_repository->createCard($postdata, $bank->id, $card_brand->id);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to create card due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process image file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Card created');

        return redirect()->action('Admin\CardController@index')->with('notification', $this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $card = $this->card_repository->getCardByUuid($uuid)->first();
        if (empty($card)) {
            return abort(404);
        }

        $banks       = $this->bank_repository
            ->orderBy('bank_nickname', 'ASC')->all(['id', 'uuid', 'bank_nickname']);
        $card_brands = $this->card_brand_repository
            ->orderBy('brand_name', 'ASC')->all(['id', 'uuid', 'brand_name', 'logo']);

        return view('admin.cards.edit', compact('card', 'banks', 'card_brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request|CardUpdateRequest $request
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(CardUpdateRequest $request, $uuid)
    {
        $card = $this->card_repository->getCardByUuid($uuid)->first();
        if (empty($card)) {
            abort(404);
        }

        $postdata = $request->all();

        $bank = $this->bank_repository->getBankByUuid($postdata['bank'], false)->first();
        if (empty($bank)) {
            return abort(404);
        }

        $card_brand = $this->card_brand_repository->getCardBrandByUuid($postdata['card_brand'])->first();
        if (empty($card_brand)) {
            return abort(404);
        }

        try {
            $this->card_repository->updateCard($postdata, $bank->id, $card_brand->id, $card);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to update card due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process image file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Card updated');

        return redirect()->action('Admin\CardController@index')->with('notification', $this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $card = $this->card_repository->getCardByUuid($uuid, false)->first();
        if (empty($card)) {
            abort(404);
        }

        if ($card->promos()->count() > 0) {
            $this->notification->error('Card can not be deleted because it have been used by existing promo');

            return redirect()->back()->with('notification', $this->notification);
        }

        try {
            $this->card_repository->deleteCard($card);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to delete card due to system error');

            return redirect()->back()->with('notification', $this->notification);
        }

        $this->notification->success('Card deleted');

        return redirect()->action('Admin\CardController@index')->with('notification', $this->notification);
    }
}
