<?php

namespace Mars\Http\Controllers\Admin\Traits;

use Mars\Exceptions\ModelException;
use Mars\Helpers\Classes\CountryHelper;
use Mars\Http\Requests\Admin\AddressStoreRequest;
use Mars\Http\Requests\Admin\AddressUpdateRequest;
use Mars\Repositories\Interfaces\AddressRepository;

trait AddressControllerTrait
{
    /**
     * @var AddressRepository
     */
    private $address_repository;

    /**
     * Show the form for creating a new resource.
     *
     * @param string $polymorphic_model_uuid
     * @return \Illuminate\Http\Response
     */
    public function createAddress($polymorphic_model_uuid)
    {
        $polymorphic_model = $this->getPolymorphicModelByUuid($polymorphic_model_uuid);
        if (empty($polymorphic_model)) {
            abort(404);
        }

        $polymorphic_edit_route = $this->getPolymorphicEditRoute($polymorphic_model_uuid);

        $address = $this->address_repository->makeModel();

        return view('admin.addresses.create', compact('address', 'polymorphic_model', 'polymorphic_edit_route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request|AddressStoreRequest $request
     * @param $polymorphic_model_uuid
     * @return \Illuminate\Http\Response
     */
    public function storeAddress(AddressStoreRequest $request, $polymorphic_model_uuid)
    {
        $polymorphic_model = $this->getPolymorphicModelByUuid($polymorphic_model_uuid);
        if (empty($polymorphic_model)) {
            abort(404);
        }

        $postdata = $request->all();

        try {
            $this->address_repository->createAddress($postdata, $polymorphic_model);
        } catch (ModelException $e) {

            $e->report();

            $this->notification->error('Unable to create address due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Address created');

        return redirect($this->getPolymorphicEditRoute($polymorphic_model_uuid))->with('notification', $this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function showAddress($uuid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param string $polymorphic_model_uuid
     * @param string $uuid
     * @return \Illuminate\Http\Response
     */
    public function editAddress($polymorphic_model_uuid, $uuid)
    {
        $polymorphic_model = $this->getPolymorphicModelByUuid($polymorphic_model_uuid);
        if (empty($polymorphic_model)) {
            abort(404);
        }

        $address = $this->address_repository->getAddressByUuidAndPolymorphicId($uuid, $polymorphic_model->id)->first();
        if (empty($address)) {
            abort(404);
        }

        $polymorphic_edit_route = $this->getPolymorphicEditRoute($polymorphic_model_uuid);

        return view('admin.addresses.edit', compact('address', 'polymorphic_model', 'polymorphic_edit_route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request|AddressUpdateRequest $request
     * @param string $polymorphic_model_uuid
     * @param string $uuid
     * @return \Illuminate\Http\Response
     */
    public function updateAddress(AddressUpdateRequest $request, $polymorphic_model_uuid, $uuid)
    {
        $polymorphic_model = $this->getPolymorphicModelByUuid($polymorphic_model_uuid);
        if (empty($polymorphic_model)) {
            abort(404);
        }

        $address = $this->address_repository->getAddressByUuidAndPolymorphicId($uuid, $polymorphic_model->id)->first();
        if (empty($address)) {
            abort(404);
        }

        $postdata = $request->all();

        try {
            $this->address_repository->updateAddress($postdata, $polymorphic_model, $address);
        } catch (ModelException $e) {

            $e->report();

            $this->notification->error('Unable to update address due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Address updated');

        return redirect($this->getPolymorphicEditRoute($polymorphic_model_uuid))->with('notification', $this->notification);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param string $polymorphic_model_uuid
     * @param string $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroyAddress($polymorphic_model_uuid, $uuid)
    {
        $polymorphic_model = $this->getPolymorphicModelByUuid($polymorphic_model_uuid);
        if (empty($polymorphic_model)) {
            abort(404);
        }

        $address = $this->address_repository->getAddressByUuidAndPolymorphicId($uuid, $polymorphic_model->id)->first();
        if (empty($address)) {
            abort(404);
        }

        try {
            $this->address_repository->deleteAddress($polymorphic_model, $address);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to delete address due to system error');

            return redirect()->back()->with('notification', $this->notification);
        }

        $this->notification->success('Address deleted');

        return redirect($this->getPolymorphicEditRoute($polymorphic_model_uuid))->with('notification', $this->notification);
    }

    public function initAddressRepository(AddressRepository $address_repository)
    {
        $this->address_repository = $address_repository;
        if (request()->isMethod('GET')) {
            app('view')->share('states', CountryHelper::getStateList());
        }
    }

    /**
     * @param string $uuid
     * @return mixed
     */
    public abstract function getPolymorphicModelByUuid($uuid);

    /**
     * @param $uuid
     * @return string
     */
    public abstract function getPolymorphicEditRoute($uuid);
}
