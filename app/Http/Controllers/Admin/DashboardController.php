<?php

namespace Mars\Http\Controllers\Admin;

use Mars\Repositories\UserRepositoryEloquent;

class DashboardController extends BaseController
{
    protected $user_repository;

    public function __construct(UserRepositoryEloquent $user_repository_eloquent)
    {
        parent::__construct();
        $this->user_repository = $user_repository_eloquent;
    }

    public function index()
    {
        return view('admin.dashboard');
    }
}
