<?php

namespace Mars\Http\Controllers\Admin;

use Mars\Exceptions\ModelException;
use Mars\Http\Requests\Admin\RoleStoreRequest;
use Mars\Http\Requests\Admin\RoleUpdateRequest;
use Mars\Repositories\Interfaces\RoleRepository;

class RoleController extends BaseController
{
    /**
     * @var RoleRepository
     */
    private $role_repository;

    /**
     * RoleController constructor.
     *
     * @param RoleRepository $role_repository
     */
    public function __construct(RoleRepository $role_repository)
    {
        parent::__construct();
        $this->role_repository = $role_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $self = app('view')->shared('self');
        if ($self->cannot('view', $this->role_repository->model())) {
            abort(403);
        }

        $roles = $this->role_repository->paginate(config('domain.general.pagination'));

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $self = app('view')->shared('self');
        if ($self->cannot('create', $this->role_repository->model())) {
            abort(403);
        }

        $role = $this->role_repository->makeModel();

        return view('admin.roles.create', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request|RoleStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleStoreRequest $request)
    {
        $self = app('view')->shared('self');
        if ($self->cannot('store', $this->role_repository->model())) {
            abort(403);
        }

        $postdata = $request->all();

        try {
            $this->role_repository->createRole($postdata, $self->id);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to create role due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Role created');

        return redirect()->action('Admin\RoleController@index')->with('notification', $this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = $this->role_repository->getRoleById($id)->first();
        if (empty($role)) {
            abort(404);
        }

        return view('admin.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request|RoleUpdateRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleUpdateRequest $request, $id)
    {
        $self = app('view')->shared('self');
        if ($self->cannot('update', $this->role_repository->model())) {
            abort(403);
        }

        $role = $this->role_repository->getRoleById($id)->first();
        if (empty($role)) {
            abort(404);
        }

        $postdata = $request->all();

        try {
            $this->role_repository->updateRole($postdata, $role, $self->id);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to update role due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Role updated');

        return redirect()->action('Admin\RoleController@index')->with('notification', $this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $self = app('view')->shared('self');
        if ($self->cannot('delete', $this->role_repository->model())) {
            abort(403);
        }

        $role = $this->role_repository->getRoleById($id)->first();
        if (empty($role)) {
            abort(404);
        }

        if (count($role->users) > 0) {
            $this->notification->error('Role can not be deleted because it have been assigned to existing user');

            return redirect()->back()->with('notification', $this->notification);
        }

        try {
            $this->role_repository->deleteRole($role);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to delete role due to system error');

            return redirect()->back()->with('notification', $this->notification);
        }

        $this->notification->success('Role deleted');

        return redirect()->action('Admin\RoleController@index')->with('notification', $this->notification);
    }
}
