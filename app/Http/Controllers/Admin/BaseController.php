<?php


namespace Mars\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Mars\Helpers\Classes\Notification;

class BaseController extends Controller
{
    protected $notification;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        if (request()->isMethod('GET')) {
            $button = config('domain.general.button');
            view()->share('button', $button);

            $this->middleware(function ($request, $next) {
                if($request->session()->has('notification')) {
                    view()->share('notification', $request->session()->get('notification'));
                }

                return $next($request);
            });
        }

        $this->notification = new Notification();

    }
}