<?php

namespace Mars\Http\Controllers\Admin;

use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Helpers\Classes\CountryHelper;
use Mars\Http\Requests\Admin\BankStoreRequest;
use Mars\Http\Requests\Admin\BankUpdateRequest;
use Mars\Repositories\Interfaces\AddressRepository;
use Mars\Repositories\Interfaces\BankRepository;

class BankController extends BaseController
{
    /**
     * @var BankRepository
     */
    private $bank_repository;
    /**
     * @var AddressRepository
     */
    private $address_repository;

    /**
     * BankController constructor.
     *
     * @param BankRepository $bank_repository
     * @param AddressRepository $address_repository
     */
    public function __construct(BankRepository $bank_repository, AddressRepository $address_repository)
    {
        parent::__construct();
        $this->bank_repository    = $bank_repository;
        $this->address_repository = $address_repository;

        if (request()->isMethod('GET')) {
            app('view')->share('states', CountryHelper::getStateList());
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banks = $this->bank_repository
            ->orderBy('bank_type', 'ASC')
            ->orderBy('bank_name', 'ASC')
            ->paginate(config('domain.general.pagination'));

        return view('admin.banks.index', compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $bank          = $this->bank_repository->makeModel();
        $bank->address = $this->address_repository->makeModel();
        $bank_type     = __('admin/lookup.bank.type');

        return view('admin.banks.create', compact('bank', 'bank_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request|BankStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BankStoreRequest $request)
    {
        $postdata = $request->all();

        try {
            $this->bank_repository->createBank($postdata);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to create bank due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process logo file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Bank created');

        return redirect()->action('Admin\BankController@index')->with('notification', $this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $bank = $this->bank_repository->getBankByUuid($uuid)->first();
        if (empty($bank)) {
            abort(404);
        }

        $bank_type = __('admin/lookup.bank.type');

        return view('admin.banks.edit', compact('bank', 'bank_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request|BankUpdateRequest $request
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(BankUpdateRequest $request, $uuid)
    {
        $bank = $this->bank_repository->getBankByUuid($uuid)->first();
        if (empty($bank)) {
            abort(404);
        }

        $postdata = $request->all();

        try {
            $this->bank_repository->updateBank($postdata, $bank);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to update bank due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process logo file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Bank updated');

        return redirect()->action('Admin\BankController@index')->with('notification', $this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $bank = $this->bank_repository->getBankByUuid($uuid)->first();
        if (empty($bank)) {
            abort(404);
        }

        if (count($bank->cards) > 0) {
            $this->notification->error('Bank can not be deleted because it have been used by existing card');

            return redirect()->back()->with('notification', $this->notification);
        }

        try {
            $this->bank_repository->deleteBank($bank);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to delete bank due to system error');

            return redirect()->back()->with('notification', $this->notification);
        }

        $this->notification->success('Bank deleted');

        return redirect()->action('Admin\BankController@index')->with('notification', $this->notification);
    }
}
