<?php

namespace Mars\Http\Controllers\Admin;

use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Http\Controllers\Admin\Traits\AddressControllerTrait;
use Mars\Http\Requests\Admin\MerchantStoreRequest;
use Mars\Http\Requests\Admin\MerchantUpdateRequest;
use Mars\Repositories\Interfaces\AddressRepository;
use Mars\Repositories\Interfaces\MerchantRepository;

class MerchantController extends BaseController
{
    use AddressControllerTrait;

    /**
     * @var MerchantRepository
     */
    private $merchant_repository;

    /**
     * MerchantController constructor.
     *
     * @param MerchantRepository $merchant_repository
     * @param AddressRepository $address_repository
     */
    public function __construct(MerchantRepository $merchant_repository, AddressRepository $address_repository)
    {
        parent::__construct();
        $this->merchant_repository = $merchant_repository;
        $this->initAddressRepository($address_repository);

        if (request()->isMethod('GET')) {
            app('view')->share('route_prefix', 'merchants');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $merchants = $this->merchant_repository->with(['addresses'])->paginate(config('domain.general.pagination'));

        return view('admin.merchants.index', compact('merchants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $merchant = $this->merchant_repository->makeModel();

        return view('admin.merchants.create', compact('merchant'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request|MerchantStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(MerchantStoreRequest $request)
    {
        $postdata = $request->all();

        try {
            $merchant = $this->merchant_repository->createMerchant($postdata);
        } catch (ModelException $e) {

            $e->report();

            $this->notification->error('Unable to create merchant due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process logo file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Merchant created');

        return redirect()->action('Admin\MerchantController@edit', $merchant->uuid)->with('notification', $this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $merchant = $this->merchant_repository->getMerchantByUuid($uuid)->first();
        if (empty($merchant)) {
            abort(404);
        }

        return view('admin.merchants.edit', compact('merchant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request|MerchantUpdateRequest $request
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(MerchantUpdateRequest $request, $uuid)
    {
        $merchant = $this->merchant_repository->getMerchantByUuid($uuid)->first();
        if (empty($merchant)) {
            abort(404);
        }

        $postdata = $request->all();

        try {
            $this->merchant_repository->updateMerchant($postdata, $merchant);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to update merchant due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process logo file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Merchant updated');

        return redirect()->action('Admin\MerchantController@index')->with('notification', $this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $merchant = $this->merchant_repository->getMerchantByUuid($uuid, false)->first();
        if (empty($merchant)) {
            abort(404);
        }

        if($merchant->promos()->count() > 0) {
            $this->notification->error('Merchant can not be deleted because it have been used by existing promo');

            return redirect()->back()->with('notification', $this->notification);
        }

        try {
            $this->merchant_repository->deleteMerchant($merchant);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to delete merchant due to system error');

            return redirect()->back()->with('notification', $this->notification);
        }

        $this->notification->success('Merchant deleted');

        return redirect()->action('Admin\MerchantController@index')->with('notification', $this->notification);
    }

    /**
     * @param string $uuid
     * @return mixed
     */
    public function getPolymorphicModelByUuid($uuid)
    {
        return $this->merchant_repository->getMerchantByUuid($uuid, false)->first();
    }

    /**
     * @param $uuid
     * @return string
     */
    public function getPolymorphicEditRoute($uuid)
    {
        return action('Admin\MerchantController@edit', $uuid);
    }
}
