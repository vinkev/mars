<?php

namespace Mars\Http\Controllers\Admin;

use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Http\Requests\Admin\PromoStoreRequest;
use Mars\Http\Requests\Admin\PromoUpdateRequest;
use Mars\Repositories\Interfaces\CardRepository;
use Mars\Repositories\Interfaces\MerchantRepository;
use Mars\Repositories\Interfaces\PromoRepository;

class PromoController extends BaseController
{
    /**
     * @var PromoRepository
     */
    private $promo_repository;
    /**
     * @var MerchantRepository
     */
    private $merchant_repository;
    /**
     * @var CardRepository
     */
    private $card_repository;

    /**
     * PromoController constructor.
     *
     * @param PromoRepository $promo_repository
     * @param MerchantRepository $merchant_repository
     * @param CardRepository $card_repository
     */
    public function __construct(PromoRepository $promo_repository, MerchantRepository $merchant_repository, CardRepository $card_repository)
    {
        parent::__construct();
        $this->promo_repository    = $promo_repository;
        $this->merchant_repository = $merchant_repository;
        $this->card_repository     = $card_repository;

        if (request()->isMethod('GET')) {
            $merchants = $this->merchant_repository->orderBy('merchant_name', 'ASC')->all(['uuid', 'merchant_name'])->pluck('merchant_name', 'uuid');
            app('view')->share('merchants', $merchants);

            $cards = $this->card_repository->with([
                'bank' => function ($query) {
                    $query->orderBy('bank_nickname');
                }
            ])->orderBy('card_name')->all();

            $grouped_cards = [];
            $cards->each(function ($card) use (&$grouped_cards) {
                $grouped_cards[$card->bank->bank_nickname][] = $card;
            });

            app('view')->share('grouped_cards', $grouped_cards);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promos = $this->promo_repository->with([
            'cards.bank' => function ($query) {
                $query->orderBy('bank_name');
            }, 'merchant'
        ])->paginate(config('admin/general.pagination'));

        return view('admin.promos.index', compact('promos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $promo           = $this->promo_repository->makeModel();
        $promo->merchant = $this->merchant_repository->makeModel();

        return view('admin.promos.create', compact('promo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request|PromoStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PromoStoreRequest $request)
    {
        $postdata = $request->all();

        $merchant = $this->merchant_repository->getMerchantByUuid($postdata['merchant'])->first();
        if (empty($merchant)) {
            abort(404);
        }

        $cards = $this->card_repository->getCardsByUuids($postdata['payment_method']);
        if ($cards->count() != count($postdata['payment_method'])) {
            abort(404);
        }

        $card_ids = $cards->pluck('id')->toArray();

        try {
            $promo = $this->promo_repository->createPromo($postdata, $merchant->id, $card_ids);
        } catch (ModelException $e) {

            $e->report();

            $this->notification->error('Unable to create promo due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process image file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Merchant created');

        return redirect()->action('Admin\MerchantController@index')->with('notification', $this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $promo = $this->promo_repository->getPromoByUuid($uuid)->first();

        return view('admin.promos.edit', compact('promo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request|PromoUpdateRequest $request
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(PromoUpdateRequest $request, $uuid)
    {
        $postdata = $request->all();

        $promo = $this->promo_repository->getPromoByUuid($uuid)->first();
        if (empty($promo)) {
            abort(404);
        }

        $merchant = $this->merchant_repository->getMerchantByUuid($postdata['merchant'])->first();
        if (empty($merchant)) {
            abort(404);
        }

        $cards = $this->card_repository->getCardsByUuids($postdata['payment_method']);
        if ($cards->count() != count($postdata['payment_method'])) {
            abort(404);
        }

        $card_ids = $cards->pluck('id')->toArray();

        try {
            $this->promo_repository->updatePromo($postdata, $merchant->id, $card_ids, $promo);
        } catch (ModelException $e) {

            $e->report();

            $this->notification->error('Unable to update promo due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process image file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Merchant updated');

        return redirect()->action('Admin\PromoController@index')->with('notification', $this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $promo = $this->promo_repository->getPromoByUuid($uuid, false)->first();
        if (empty($promo)) {
            abort(404);
        }

        try {
            $this->promo_repository->deletePromo($promo);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to delete promo due to system error');

            return redirect()->back()->with('notification', $this->notification);
        }

        $this->notification->success('Promo deleted');

        return redirect()->action('Admin\PromoController@index')->with('notification', $this->notification);
    }
}
