<?php

namespace Mars\Http\Controllers\Admin;

use Mars\Exceptions\FileUploadException;
use Mars\Exceptions\ModelException;
use Mars\Http\Requests\Admin\CardBrandStoreRequest;
use Mars\Http\Requests\Admin\CardBrandUpdateRequest;
use Mars\Repositories\Interfaces\CardBrandRepository;

class CardBrandController extends BaseController
{
    /**
     * @var CardBrandRepository
     */
    private $card_brand_repository;

    /**
     * CardBrandController constructor.
     *
     * @param CardBrandRepository $card_brand_repository
     */
    public function __construct(CardBrandRepository $card_brand_repository)
    {
        parent::__construct();

        $this->card_brand_repository = $card_brand_repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $card_brands = $this->card_brand_repository->paginate(config('domain.general.pagination'));

        return view('admin.cards.brands.index', compact('card_brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $card_brand = $this->card_brand_repository->makeModel();

        return view('admin.cards.brands.create', compact('card_brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request|CardBrandStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CardBrandStoreRequest $request)
    {
        $postdata = $request->all();

        try {
            $this->card_brand_repository->createCardBrand($postdata);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to create card brand due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process logo file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Card brand created');

        return redirect()->action('Admin\CardBrandController@index')->with('notification', $this->notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        $card_brand = $this->card_brand_repository->getCardBrandByUuid($uuid)->first();
        if (empty($card_brand)) {
            abort(404);
        }

        return view('admin.cards.brands.edit', compact('card_brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request|CardBrandUpdateRequest $request
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function update(CardBrandUpdateRequest $request, $uuid)
    {
        $card_brand = $this->card_brand_repository->getCardBrandByUuid($uuid)->first();
        if (empty($card_brand)) {
            abort(404);
        }

        $postdata = $request->all();

        try {
            $this->card_brand_repository->updateCardBrand($postdata, $card_brand);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to update card brand due to system error');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        } catch (FileUploadException $e) {
            $e->report();

            $this->notification->error('Unable to process logo file. Please try again.');

            return redirect()->back()->with('notification', $this->notification)->withInput();
        }

        $this->notification->success('Card brand updated');

        return redirect()->action('Admin\CardBrandController@index')->with('notification', $this->notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $uuid
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        $card_brand = $this->card_brand_repository->getCardBrandByUuid($uuid)->first();
        if (empty($card_brand)) {
            abort(404);
        }

        if (count($card_brand->cards) > 0) {
            $this->notification->error('Card brand can not be deleted because it have been used by existing card');

            return redirect()->back()->with('notification', $this->notification);
        }

        try {
            $this->card_brand_repository->deleteCardBrand($card_brand);
        } catch (ModelException $e) {
            $e->report();

            $this->notification->error('Unable to delete card brand due to system error');

            return redirect()->back()->with('notification', $this->notification);
        }

        $this->notification->success('Card brand deleted');

        return redirect()->action('Admin\CardBrandController@index')->with('notification', $this->notification);
    }
}
