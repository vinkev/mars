<?php


namespace Mars\Traits;


use Auth;

trait AuditTrait
{
    public static function bootAuditTrait()
    {
        static::creating(function($model){
            if(empty($model->created_by)) {
                if(Auth::check()) {
                    $self = app('view')->shared('self');
                    $model->created_by = $self->id;
                } else {
                    $model->created_by = 0;
                }
            }
        });

        static::updating(function($model){
            if(empty($model->updated_by)) {
                if(Auth::check()) {
                    $self = app('view')->shared('self');
                    $model->updated_by = $self->id;
                } else {
                    $model->updated_by = 0;
                }
            }
        });
    }
}