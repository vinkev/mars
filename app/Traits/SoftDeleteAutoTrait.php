<?php

namespace Mars\Traits;

use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

trait SoftDeleteAutoTrait
{
    use SoftDeletes;

    public static function bootSoftDeleteAutoTrait()
    {
        static::deleting(function($model){
            if(empty($model->deleted_by)) {
                if(Auth::check()) {
                    $self = app('view')->shared('self');
                    $model->deleted_by = $self->id;
                } else {
                    $model->deleted_by = 0;
                }
                $model->save();
            }
        });
    }
}