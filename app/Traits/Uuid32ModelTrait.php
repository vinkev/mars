<?php

namespace Mars\Traits;

use Ramsey\Uuid\Uuid;

trait Uuid32ModelTrait
{
    public static function bootUuid32ModelTrait()
    {
        static::creating(function ($model) {
            if(empty($model->uuid)){
                $model->uuid = str_replace('-', '', Uuid::uuid4());
            }
        });
    }
}