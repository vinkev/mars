<?php


namespace Mars\Exceptions;


use Auth;
use Exception;
use Throwable;

class FileUploadException extends Exception
{
    protected $detailed_message;

    public function __construct($message = "", $code = 0, Throwable $previous = null, $detailed_message = "")
    {
        parent::__construct($message, $code, $previous);
        $this->detailed_message = $detailed_message;
    }

    public function report()
    {
        $context = [
            'exception'      => $this->getMessage(),
            'exception_file' => $this->getFile() . ':' . $this->getLine(),
            'postdata'       => request()->all(),
        ];

        if (Auth::check()) {
            $context['user_id'] = Auth::id();
        }

        app('log')->error($this->detailed_message, $context);
    }
}