<?php

namespace Mars\Transformers;

use League\Fractal\TransformerAbstract;
use Mars\Models\UserProfile;

/**
 * Class UserProfileTransformer
 * @package namespace Mars\Transformers;
 */
class UserProfileTransformer extends TransformerAbstract
{

    /**
     * Transform the \UserProfile entity
     * @param \UserProfile $model
     *
     * @return array
     */
    public function transform(UserProfile $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
