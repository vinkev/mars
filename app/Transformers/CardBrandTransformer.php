<?php

namespace Mars\Transformers;

use League\Fractal\TransformerAbstract;
use Mars\Models\CardBrand;

/**
 * Class CardBrandTransformer
 * @package namespace Mars\Transformers;
 */
class CardBrandTransformer extends TransformerAbstract
{

    /**
     * Transform the \CardBrand entity
     * @param \CardBrand $model
     *
     * @return array
     */
    public function transform(CardBrand $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
