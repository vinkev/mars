<?php

namespace Mars\Transformers;

use League\Fractal\TransformerAbstract;
use Mars\Models\Card;

/**
 * Class CardTransformer
 * @package namespace Mars\Transformers;
 */
class CardTransformer extends TransformerAbstract
{

    /**
     * Transform the \Card entity
     * @param \Card $model
     *
     * @return array
     */
    public function transform(Card $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
