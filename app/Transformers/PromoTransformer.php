<?php

namespace Mars\Transformers;

use League\Fractal\TransformerAbstract;
use Mars\Models\Promo;

/**
 * Class PromoTransformer
 * @package namespace Mars\Transformers;
 */
class PromoTransformer extends TransformerAbstract
{

    /**
     * Transform the \Promo entity
     * @param \Promo $model
     *
     * @return array
     */
    public function transform(Promo $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
