<?php

namespace Mars\Transformers;

use League\Fractal\TransformerAbstract;
use Mars\Models\Merchant;

/**
 * Class MerchantTransformer
 * @package namespace Mars\Transformers;
 */
class MerchantTransformer extends TransformerAbstract
{

    /**
     * Transform the \Merchant entity
     * @param \Merchant $model
     *
     * @return array
     */
    public function transform(Merchant $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
