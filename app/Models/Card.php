<?php

namespace Mars\Models;

use Illuminate\Database\Eloquent\Model;
use Mars\Helpers\Constants\DBTable;
use Mars\Traits\AuditTrait;
use Mars\Traits\SoftDeleteAutoTrait;
use Mars\Traits\Uuid32ModelTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Mars\Models\Card
 *
 * @property int $id
 * @property string $uuid
 * @property int $bank_id
 * @property int $card_brand_id
 * @property string $card_name
 * @property int $card_type
 * @property string $image
 * @property int $is_enabled
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Mars\Models\Bank $bank
 * @property-read \Mars\Models\CardBrand $brand
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mars\Models\Promo[] $promos
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereBankId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereCardBrandId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereCardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereCardType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereIsEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Card whereUuid($value)
 * @mixin \Eloquent
 */
class Card extends Model implements Transformable
{
    use TransformableTrait;
    use Uuid32ModelTrait;
    use SoftDeleteAutoTrait;
    use AuditTrait;

    protected $table = DBTable::CARDS;

    protected $fillable = ['bank_id', 'card_brand_id', 'card_name', 'card_type', 'image'];

    public function bank(){
        return $this->belongsTo(Bank::class, 'bank_id');
    }

    public function brand(){
        return $this->belongsTo(CardBrand::class, 'card_brand_id');
    }

    public function promos(){
        return $this->morphToMany(Promo::class, 'payment_method', DBTable::PROMO_PAYMENT_METHODS);
    }
}
