<?php

namespace Mars\Models;

use Illuminate\Database\Eloquent\Model;
use Mars\Helpers\Constants\DBTable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Mars\Models\UserProfile
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property int $gender {"MALE":1,"FEMALE":2,"OTHER":3}
 * @property array $cell_phone
 * @property string|null $photo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Mars\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\UserProfile whereCellPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\UserProfile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\UserProfile whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\UserProfile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\UserProfile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\UserProfile wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\UserProfile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\UserProfile whereUserId($value)
 * @mixin \Eloquent
 */
class UserProfile extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = DBTable::USER_PROFILES;

    protected $fillable = [];

    protected $casts = [
        'cell_phone' => 'array'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
