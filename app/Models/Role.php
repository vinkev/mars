<?php

namespace Mars\Models;

use Illuminate\Database\Eloquent\Model;
use Mars\Helpers\Constants\DBTable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Silber\Bouncer\Database\Concerns\IsRole;

/**
 * Mars\Models\Role
 *
 * @property int $id
 * @property string $name
 * @property string|null $title
 * @property int|null $level
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Ability[] $abilities
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role whereAssignedTo($model, $keys = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Role whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Role extends Model implements Transformable
{
    use TransformableTrait;
    use IsRole;

    const SUPER_ADMIN = 'super-admin';
    const ADMIN = 'admin';
    const USER = 'user';

    protected $table = DBTable::ROLES;

    protected $fillable = ['name', 'title'];


}
