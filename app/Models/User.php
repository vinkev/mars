<?php

namespace Mars\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Mars\Traits\Uuid32ModelTrait;
use Prettus\Repository\Traits\TransformableTrait;
use Ramsey\Uuid\Uuid;
use Silber\Bouncer\Database\HasRolesAndAbilities;

/**
 * Mars\Models\User
 *
 * @property int $id
 * @property string $uuid
 * @property string $email
 * @property string $password
 * @property int $user_status {"PENDING":1,"ACTIVE":2,"INACTIVE":3,"BANNED":4}
 * @property string|null $remember_token
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Ability[] $abilities
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Mars\Models\UserProfile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\Silber\Bouncer\Database\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereIs($role)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereIsAll($role)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereIsNot($role)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereUserStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\User whereUuid($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRolesAndAbilities;
    use TransformableTrait;
    use Uuid32ModelTrait;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(UserProfile::class,'user_id')->withDefault();;
    }

}
