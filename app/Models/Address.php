<?php

namespace Mars\Models;

use Illuminate\Database\Eloquent\Model;
use Mars\Helpers\Constants\DBTable;
use Mars\Traits\AuditTrait;
use Mars\Traits\SoftDeleteAutoTrait;
use Mars\Traits\Uuid32ModelTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Mars\Models\Address
 *
 * @property int $id
 * @property string $uuid
 * @property int $addressable_id
 * @property string $addressable_type
 * @property string|null $location_name
 * @property string|null $location_info
 * @property string $address
 * @property string|null $city
 * @property string|null $state
 * @property string $country
 * @property string|null $zip_code
 * @property array $office_phone
 * @property array $fax_phone
 * @property int $is_enabled
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $addressable
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereAddressableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereAddressableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereFaxPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereIsEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereLocationInfo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereLocationName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereOfficePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereState($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Address whereZipCode($value)
 * @mixin \Eloquent
 */
class Address extends Model implements Transformable
{
    use TransformableTrait;
    use Uuid32ModelTrait;
    use SoftDeleteAutoTrait;
    use AuditTrait;

    protected $table = DBTable::ADDRESSES;

    protected $fillable = ['location_name', 'location_info', 'address', 'city', 'zip_code', 'office_phone', 'fax_phone'];

    protected $casts = [
        'office_phone' => 'array',
        'fax_phone' => 'array'
    ];

    public function addressable(){
        return $this->morphTo();
    }

}
