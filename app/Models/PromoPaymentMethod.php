<?php

namespace Mars\Models;

use Illuminate\Database\Eloquent\Model;
use Mars\Helpers\Constants\DBTable;
use Mars\Traits\AuditTrait;
use Mars\Traits\SoftDeleteAutoTrait;

/**
 * Mars\Models\PromoPaymentMethod
 *
 * @property int $promo_id
 * @property int $payment_method_id
 * @property string $payment_method_type
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\PromoPaymentMethod whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\PromoPaymentMethod whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\PromoPaymentMethod whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\PromoPaymentMethod whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\PromoPaymentMethod wherePaymentMethodId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\PromoPaymentMethod wherePaymentMethodType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\PromoPaymentMethod wherePromoId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\PromoPaymentMethod whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\PromoPaymentMethod whereUpdatedBy($value)
 * @mixin \Eloquent
 */
class PromoPaymentMethod extends Model
{
    use SoftDeleteAutoTrait;
    use AuditTrait;

    protected $table = DBTable::PROMO_PAYMENT_METHODS;
}
