<?php

namespace Mars\Models;

use Illuminate\Database\Eloquent\Model;
use Mars\Helpers\Constants\DBTable;
use Mars\Traits\AuditTrait;
use Mars\Traits\SoftDeleteAutoTrait;
use Mars\Traits\Uuid32ModelTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Mars\Models\Bank
 *
 * @property int $id
 * @property string $uuid
 * @property string $bank_name
 * @property string $bank_nickname
 * @property int $bank_type {"BANK_UMUM_PERSERO":1,"BANK_UMUM_SWASTA_NASIONAL":2,"BANK_PEMBANGUNAN_DAERAH":3,"BANK_ASING":4}
 * @property string $logo
 * @property string $transfer_bank_code
 * @property string|null $website
 * @property string|null $description
 * @property int $is_enabled
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Mars\Models\Address $address
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mars\Models\Card[] $cards
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereBankName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereBankNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereBankType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereIsEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereTransferBankCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Bank whereWebsite($value)
 * @mixin \Eloquent
 */
class Bank extends Model implements Transformable
{
    use TransformableTrait;
    use Uuid32ModelTrait;
    use SoftDeleteAutoTrait;
    use AuditTrait;

    protected $table = DBTable::BANKS;

    protected $fillable = ['bank_name', 'bank_nickname', 'bank_type', 'logo', 'transfer_bank_code', 'website', 'description', 'is_enabled'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function cards()
    {
        return $this->hasMany(Card::class, 'bank_id');
    }

    public function address()
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    public function repositoryInterface()
    {
        return app('Mars\Repositories\Interfaces\BankRepository');
    }
}
