<?php

namespace Mars\Models;

use Illuminate\Database\Eloquent\Model;
use Mars\Helpers\Constants\DBTable;
use Mars\Traits\AuditTrait;
use Mars\Traits\SoftDeleteAutoTrait;
use Mars\Traits\Uuid32ModelTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Mars\Models\Merchant
 *
 * @property int $id
 * @property string $uuid
 * @property string $merchant_name
 * @property string|null $logo
 * @property string|null $website
 * @property string|null $description
 * @property int $is_enabled
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mars\Models\Address[] $addresses
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mars\Models\Promo[] $promos
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereIsEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereMerchantName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereUuid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Merchant whereWebsite($value)
 * @mixin \Eloquent
 */
class Merchant extends Model implements Transformable
{
    use TransformableTrait;
    use Uuid32ModelTrait;
    use SoftDeleteAutoTrait;
    use AuditTrait;

    protected $table = DBTable::MERCHANTS;
    protected $fillable = ['merchant_name', 'logo', 'website', 'description', 'is_enabled'];

    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    public function promos()
    {
        return $this->hasMany(Promo::class, 'merchant_id');
    }

    public function repositoryInterface()
    {
        return app('Mars\Repositories\Interfaces\MerchantRepository');
    }
}
