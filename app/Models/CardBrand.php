<?php

namespace Mars\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Mars\Helpers\Constants\DBTable;
use Mars\Traits\AuditTrait;
use Mars\Traits\SoftDeleteAutoTrait;
use Mars\Traits\Uuid32ModelTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Mars\Models\CardBrand
 *
 * @property int $id
 * @property string $uuid
 * @property string $brand_name
 * @property string|null $description
 * @property string $logo
 * @property int $is_enabled
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Mars\Models\Card[] $cards
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereBrandName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereIsEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereLogo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\CardBrand whereUuid($value)
 * @mixin \Eloquent
 */
class CardBrand extends Model implements Transformable
{
    use TransformableTrait;
    use Uuid32ModelTrait;
    use SoftDeleteAutoTrait;
    use AuditTrait;

    protected $table = DBTable::CARD_BRANDS;

    protected $fillable = ['is_enabled', 'brand_name', 'logo', 'description'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function cards(){
        return $this->hasMany(Card::class, 'card_brand_id');
    }
}
