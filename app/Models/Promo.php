<?php

namespace Mars\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Mars\Helpers\Constants\DBTable;
use Mars\Traits\AuditTrait;
use Mars\Traits\SoftDeleteAutoTrait;
use Mars\Traits\Uuid32ModelTrait;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Mars\Models\Promo
 *
 * @property int $id
 * @property string $uuid
 * @property int $merchant_id
 * @property string $promo_title
 * @property string|null $promo_subtitle
 * @property string|null $description
 * @property string|null $term_condition
 * @property string|null $image
 * @property \Carbon\Carbon|null $publish_at
 * @property \Carbon\Carbon|null $start_at
 * @property \Carbon\Carbon|null $end_at
 * @property int $is_enabled
 * @property int $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read mixed $is_active
 * @property-read \Mars\Models\Merchant $merchant
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereEndAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereIsEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereMerchantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo wherePromoSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo wherePromoTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo wherePublishAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereTermCondition($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereUpdatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Mars\Models\Promo whereUuid($value)
 * @mixin \Eloquent
 */
class Promo extends Model implements Transformable
{
    use TransformableTrait;
    use SoftDeleteAutoTrait;
    use Uuid32ModelTrait;
    use AuditTrait;

    protected $table = DBTable::PROMOS;
    protected $fillable = ['is_enabled','merchant_id', 'promo_title', 'promo_subtitle', 'publish_at', 'start_at', 'end_at', 'description', 'term_condition', 'image'];
    protected $dates = ['publish_at', 'start_at', 'end_at', 'created_at', 'updated_at', 'deleted_at'];

    public function cards()
    {
        return $this->morphedByMany(Card::class, 'payment_method', DBTable::PROMO_PAYMENT_METHODS);
    }

    public function getBanksAttribute()
    {
        $banks = collect();
        foreach($this->cards as $card) {
            $banks->put($card->bank->id, $card->bank);
        }
        return $banks->values();
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class,'id', 'merchant_id');
    }

    public function getIsComingSoonAttribute()
    {
        return Carbon::now()->lt($this->start_at);
    }

    public function getIsActiveAttribute()
    {
        return Carbon::now()->gte($this->start_at) && Carbon::now()->lte($this->end_at);
    }

    public function getIsExpiredAttribute()
    {
        return Carbon::now()->gt($this->end_at);
    }

    /**
     * Check if card id is exist on cards
     *
     * @param int $card_id
     * @return bool
     */
    public function isCardSelected($card_id)
    {
        return $this->cards->contains(function($card, $key) use ($card_id){
            return $card->id == $card_id;
        });
    }

}
