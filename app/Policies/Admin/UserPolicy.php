<?php

namespace Mars\Policies\Admin;

use Mars\Models\Role;
use Mars\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before($user, $ability)
    {
        if ($user->isA(Role::SUPER_ADMIN)) {
            return true;
        }
    }

}
