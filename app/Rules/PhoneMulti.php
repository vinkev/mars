<?php

namespace Mars\Rules;

use Illuminate\Contracts\Validation\Rule;
use Propaganistas\LaravelPhone\Validation\Phone;

class PhoneMulti implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  stri2ng  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $phones = preg_split("/\\r\\n|\\r|\\n/", $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be a phone number format.';
    }
}
