<?php

namespace Mars\Helpers\Meta\Factories;

use Mars\Helpers\Meta\Traits\MetaFactory;
use Mars\Helpers\Meta\Entities\MetaProperty;

class MetaPropertyFactory
{
    use MetaFactory;

    public function __construct()
    {
        $this->entity =  MetaProperty::class;
    }
}