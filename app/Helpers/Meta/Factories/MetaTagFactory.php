<?php
namespace Mars\Helpers\Meta\Factories;

use Mars\Helpers\Meta\Traits\MetaFactory;
use Mars\Helpers\Meta\Entities\MetaTag;

class MetaTagFactory
{
    use MetaFactory;

    public function __construct()
    {
        $this->entity =  MetaTag::class;
    }
}