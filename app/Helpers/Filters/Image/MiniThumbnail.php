<?php

namespace Mars\Helpers\Filters\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class MiniThumbnail implements FilterInterface {
    
	public function applyFilter(Image $image)
    {
//        return $image->fit(40, 40, function ($constraint) {
//            $constraint->upsize();
//        });
        $image->resize(40, 40, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $image->resizeCanvas(40, 40, 'center', false, array(255, 255, 255, 0));

        return $image;
    }
}