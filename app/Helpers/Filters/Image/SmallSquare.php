<?php

namespace Mars\Helpers\Filters\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class SmallSquare implements FilterInterface {
    
	public function applyFilter(Image $image)
    {
        $image->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        
        $image->resizeCanvas(200, 200, 'center', false, array(255, 255, 255, 0));
        return $image;
    }
}