<?php

namespace Mars\Helpers\Filters\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class MediumSquare implements FilterInterface {
    public function applyFilter(Image $image)
    {
        $image->resize(400, 400, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        
        $image->resizeCanvas(400, 400, 'center', false, array(255, 255, 255, 0));
        
        return $image;
    }
}