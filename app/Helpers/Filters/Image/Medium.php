<?php

namespace Mars\Helpers\Filters\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Medium implements FilterInterface {
	
    public function applyFilter(Image $image)
    {
        return $image->widen(400, function ($constraint) {
            $constraint->upsize();
        });
    }
}