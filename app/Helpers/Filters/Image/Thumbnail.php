<?php

namespace Mars\Helpers\Filters\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Thumbnail implements FilterInterface {
    
	public function applyFilter(Image $image)
    {
        $image->resize(100, 100, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        
        $image->resizeCanvas(100, 100, 'center', false, array(255, 255, 255, 0));
        
        return $image;
    }
}