<?php

namespace Mars\Helpers\Filters\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Large implements FilterInterface {
	
    public function applyFilter(Image $image)
    {
        return $image->widen(600, function ($constraint) {
            $constraint->upsize();
        });
    }
}