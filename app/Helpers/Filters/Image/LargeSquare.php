<?php

namespace Mars\Helpers\Filters\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class LargeSquare implements FilterInterface {
    
	public function applyFilter(Image $image)
    {
        $image->resize(600, 600, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $image->resizeCanvas(600, 600, 'center', false, array(255, 255, 255, 0));
        
        return $image;
    }
}