<?php

namespace Mars\Helpers\Filters\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Small implements FilterInterface {
    
	public function applyFilter(Image $image)
    {
        return $image->widen(200, function ($constraint) {
            $constraint->upsize();
        });
    }
}