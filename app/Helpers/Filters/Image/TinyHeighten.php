<?php


namespace Mars\Helpers\Filters\Image;


use Intervention\Image\Filters\FilterInterface;

class TinyHeighten implements FilterInterface
{

    /**
     * Applies filter to given image
     *
     * @param  \Intervention\Image\Image $image
     * @return \Intervention\Image\Image
     */
    public function applyFilter(\Intervention\Image\Image $image)
    {
        return $image->heighten(40, function ($constraint) {
            $constraint->upsize();
        });

        return $image;
    }
}