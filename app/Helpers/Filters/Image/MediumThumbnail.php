<?php

namespace Mars\Helpers\Filters\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class MediumThumbnail implements FilterInterface {
	
    public function applyFilter(Image $image)
    {
        $image->resize(70, 70, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        
        $image->resizeCanvas(70, 70, 'center', false, array(255, 255, 255, 0));
        
        return $image;
    }
}