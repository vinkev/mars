<?php

namespace Mars\Helpers\Classes;

use Mars\Helpers\Constants\ImageTemplate;

class ImageHelper
{
    /*
     * Thumbnail
     */

    public static function thumbnail($image_file)
    {
        return self::cdnPath(ImageTemplate::THUMBNAIL, $image_file);
    }

    public static function mediumThumbnail($image_file)
    {
        return self::cdnPath(ImageTemplate::MEDIUM_THUMBNAIL, $image_file);
    }

    public static function miniThumbnail($image_file)
    {
        return self::cdnPath(ImageTemplate::MINI_THUMBNAIL, $image_file);
    }


    /*
     * Tiny
     */

    public static function tinyHeighten($image_file)
    {
        return self::cdnPath(ImageTemplate::TINY_HEIGHTEN, $image_file);
    }


    /*
     * Small
     */

    public static function smallSquare($image_file)
    {
        return self::cdnPath(ImageTemplate::SMALL_SQUARE, $image_file);
    }

    public static function small($image_file)
    {
        return self::cdnPath(ImageTemplate::SMALL, $image_file);
    }

    /*
     * Medium
     */

    public static function mediumSquare($image_file)
    {
        return self::cdnPath(ImageTemplate::MEDIUM_SQUARE, $image_file);
    }

    public static function medium($image_file)
    {
        return self::cdnPath(ImageTemplate::MEDIUM, $image_file);
    }


    /*
     * Large
     */

    public static function largeSquare($image_file)
    {
        return self::cdnPath(ImageTemplate::LARGE_SQUARE, $image_file);
    }

    public static function large($image_file)
    {
        return self::cdnPath(ImageTemplate::LARGE, $image_file);
    }

    /*
     * Original
     */

    public static function original($image_file)
    {
        return self::cdnPath(ImageTemplate::ORIGINAL, $image_file);
    }


    public static function defaultImageArray()
    {
        return [
            'is_default'          => 1,
            'display_order'       => 1,
            'caption'             => 'No Image',
            'image_url'           => self::largeProductDefault(),
            'image_thumbnail_url' => self::thumbnailProductDefault(),
        ];
    }


    /*
     * Helper
     */

    private static function cdnPath($template, $image_file)
    {
        if (!starts_with($image_file, '/')) {
            $image_file = '/' . $image_file;
        }

        return asset('cdn/' . $template . $image_file);
    }

}