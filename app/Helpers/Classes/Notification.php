<?php


namespace Mars\Helpers\Classes;


class Notification
{
    public $type;
    public $message;
    public $delay;

    /**
     * Notification constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param string $type
     * @param string $message
     * @param int $delay
     * @return $this
     */
    public function build($type, $message, $delay = 8000)
    {
        $this->type    = $type;
        $this->message = $message;
        $this->delay   = $delay;

        return $this;
    }

    public function error($message, $delay = 8000)
    {
        $this->type    = 'error';
        $this->message = $message;
        $this->delay   = $delay;

        return $this;
    }

    public function warning($message, $delay = 8000)
    {
        $this->type    = 'warning';
        $this->message = $message;
        $this->delay   = $delay;

        return $this;
    }

    public function success($message, $delay = 8000)
    {
        $this->type    = 'success';
        $this->message = $message;
        $this->delay   = $delay;

        return $this;
    }
}
