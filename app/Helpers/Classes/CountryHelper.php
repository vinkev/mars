<?php

namespace Mars\Helpers\Classes;

use Countries;

class CountryHelper
{
    public static function getStateList()
    {
        $states = Countries::where('cca2', 'ID')->first()->states->sortBy('name')->pluck('name', 'postal');
        $states->shift();

        return $states;
    }

    public static function getCountryList()
    {

    }
}