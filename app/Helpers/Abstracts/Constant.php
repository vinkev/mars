<?php

namespace Mars\Helpers\Abstracts;

use ReflectionClass;

abstract class Constant
{
    /**
     * Return array of constants for a class
     *
     * @param null|string $prefix Prefix like e.g. "KEY_"
     * @param boolean     $assoc  Return associative array with constant name as key
     * @param array       $exclude Array of excluding values
     *
     * @return array Assoc array of constants
     */
    public static function getConstants($prefix = null, $assoc = false, $exclude = [])
    {
        $reflector = new ReflectionClass(get_called_class());
        $constants = $reflector->getConstants();
        $values = [];

        foreach ($constants as $constant => $value) {
        	
            if (($prefix && strpos($constant, $prefix) !==false) || $prefix === null) {
                
            	if (in_array($value, $exclude)) {
                    continue;
                }
                
                if ($assoc) {
                    $values[$constant] = $value;
                } else {
                    $values[] = $value;
                }
            }
        }

        return $values;
    }

    /**
     * Return array of static properties for a class
     *
     * @param null|string $prefix Prefix like e.g. "KEY_"
     * @param boolean     $assoc  Return associative array with constant name as key
     * @param array       $exclude Array of excluding values
     *
     * @return array Assoc array of constants
     */
    public static function getStaticProperties($prefix = null, $assoc = false, $exclude = [])
    {
        $reflector = new ReflectionClass(get_called_class());
        $properties = $reflector->getStaticProperties();

        $values = [];

        foreach ($properties as $property => $value) {
        	
            if (($prefix && strpos($property, $prefix) !==false) || $prefix === null) {

            	if (in_array($value, $exclude)) {
                    continue;
                }
                
                if ($assoc) {
                    $values[$property] = $value;
                } else {
                    $values[] = $value;
                }
            }
        }

        return $values;
    }


    /**
     * Check if constant with certain value exist
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function hasValue($value)
    {
    	$constants = self::getConstants();
        
        if (in_array($value, $constants)) {
            return true;
        }
        
        return false;
    }
}