<?php

namespace Mars\Helpers\Constants;

use Mars\Helpers\Abstracts\Constant;

class BankType extends Constant
{
    const BANK_UMUM_PERSERO = 1;
    const BANK_UMUM_SWASTA_NASIONAL = 2;
    const BANK_PEMBANGUNAN_DAERAH = 3;
    const BANK_ASING = 4;
}