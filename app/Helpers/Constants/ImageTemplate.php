<?php


namespace Mars\Helpers\Constants;


class ImageTemplate
{
    const MINI_THUMBNAIL   = 'mini-thumbnail';
    const MEDIUM_THUMBNAIL = 'medium-thumbnail';
    const THUMBNAIL        = 'thumbnail';
    const TINY_HEIGHTEN    = 'tiny-heighten';
    const SMALL            = 'small';
    const SMALL_SQUARE     = 'small-square';
    const MEDIUM           = 'medium';
    const MEDIUM_SQUARE    = 'medium-square';
    const LARGE            = 'large';
    const LARGE_SQUARE     = 'large-square';
    const ORIGINAL         = 'original';
}