<?php

namespace Mars\Helpers\Constants;

use Mars\Helpers\Abstracts\Constant;

class Gender extends Constant
{
    const MALE   = 1;
    const FEMALE = 2;
    const OTHER  = 3;
}