<?php

namespace Mars\Helpers\Constants;

use Mars\Helpers\Abstracts\Constant;

class CardType extends Constant
{
    const CREDIT_CARD = 1;
    const DEBIT_CARD = 2;
}