<?php

namespace Mars\Helpers\Constants;

use Mars\Helpers\Abstracts\Constant;

class UserStatus extends Constant
{
    const PENDING = 1;
    const ACTIVE = 2;
    const INACTIVE = 3;
    const BANNED = 4;
}