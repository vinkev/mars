<?php

namespace Mars\Helpers\Constants;

use Mars\Helpers\Abstracts\Constant;

class DBTable extends Constant
{
    const ROLES         = 'roles';
    const USERS         = 'users';
    const USER_PROFILES = 'user_profiles';

    const CARDS       = 'cards';
    const CARD_BRANDS = 'card_brands';
    const BANKS       = 'banks';
    const MERCHANTS   = 'merchants';
    const ADDRESSES   = 'addresses';

    const PROMOS                = 'promos';
    const PROMO_PAYMENT_METHODS = 'promo_payment_methods';

}