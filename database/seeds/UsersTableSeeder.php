<?php

use Illuminate\Database\Seeder;
use Mars\Helpers\Constants\DBTable;
use Mars\Helpers\Constants\Gender;
use Mars\Helpers\Constants\UserStatus;
use Mars\Models\UserProfile;
use Mars\Repositories\UserProfileRepositoryEloquent;
use Mars\Repositories\UserRepositoryEloquent;

class UsersTableSeeder extends Seeder
{
    protected $user_repository;
    protected $user_profile_repository;

    public function __construct(UserRepositoryEloquent $user_repository, UserProfileRepositoryEloquent $user_profile_repository){
        $this->user_repository = $user_repository;
        $this->user_profile_repository = $user_profile_repository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table(DBTable::USER_PROFILES)->truncate();
        DB::table(DBTable::USERS)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $user = $this->user_repository->firstOrCreate([
            'email'    => 'kevin.linggajaya@gmail.com',
            'password' => bcrypt('test1234'),
            'user_status' => UserStatus::ACTIVE
        ]);

        $user_profile = new UserProfile();
        $user_profile->name = 'Kevin Linggajaya';
        $user_profile->cell_phone = [
            'phone_number' => '+6281808078020',
            'verified' => false
        ];
        $user_profile->gender = Gender::MALE;
        $user_profile->user()->associate($user);
        $user_profile->save();

        Bouncer::assign('super-admin')->to($user);
    }
}
