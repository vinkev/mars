<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Mars\Helpers\Constants\DBTable;

class CreatePromoPaymentMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable(DBTable::PROMO_PAYMENT_METHODS)) {
            Schema::create(DBTable::PROMO_PAYMENT_METHODS, function (Blueprint $table) {
                $table->bigInteger('promo_id')->unsigned()->index();
                $table->foreign('promo_id')->references('id')->on(DBTable::PROMOS);
                $table->bigInteger('payment_method_id')->unsigned()->index();
                $table->string('payment_method_type')->index();

                $table->timestampTz('created_at')->useCurrent();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::PROMO_PAYMENT_METHODS);
    }
}
