<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Mars\Helpers\Constants\DBTable;
use Mars\Helpers\Constants\Gender;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable(DBTable::USER_PROFILES)) {
            Schema::create(DBTable::USER_PROFILES, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('user_id')->unsigned();
                $table->foreign('user_id')->references('id')->on(DBTable::USERS);
                $table->string('name', 48);
                $table->tinyInteger('gender')->comment(json_encode(Gender::getConstants(null, true)));
                $table->json('cell_phone')->nullable()->default(null);
                $table->string('photo', 64)->nullable()->default(null);
                $table->bigInteger('created_by')->unsigned()->default(0);
                $table->bigInteger('updated_by')->unsigned()->default(null);
                $table->timestampTz('created_at')->useCurrent();
                $table->timestampTz('updated_at')->nullable()->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::USER_PROFILES);
    }
}
