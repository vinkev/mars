<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Mars\Helpers\Constants\CardType;
use Mars\Helpers\Constants\DBTable;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(DBTable::CARDS)) {
            Schema::create(DBTable::CARDS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('uuid', 32)->index();
                $table->bigInteger('bank_id')->unsigned();
                $table->foreign('bank_id')->references('id')->on(DBTable::BANKS);
                $table->bigInteger('card_brand_id')->unsigned();
                $table->foreign('card_brand_id')->references('id')->on(DBTable::CARD_BRANDS);
                $table->string('card_name', 255);
                $table->tinyInteger('card_type')->comments(json_encode(CardType::getConstants(null, true)));
                $table->string('image', 80);
                $table->boolean('is_enabled')->default(1);

                $table->bigInteger('created_by')->unsigned()->default(0);
                $table->bigInteger('updated_by')->unsigned()->nullable()->default(null);
                $table->bigInteger('deleted_by')->unsigned()->nullable()->default(null);
                $table->timestampTz('created_at')->useCurrent();
                $table->timestampTz('updated_at')->nullable()->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
                $table->timestampTz('deleted_at')->nullable()->default(null);

                $table->unique(['bank_id', 'card_name', 'deleted_at']);

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::CARDS);
    }
}
