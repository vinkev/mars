<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Mars\Helpers\Constants\DBTable;

class CreateMerchantsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(DBTable::MERCHANTS)) {
            Schema::create(DBTable::MERCHANTS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('uuid', 32)->index();
                $table->string('merchant_name');
                $table->string('logo')->nullable()->default(null);
                $table->string('website')->nullable()->default(null);
                $table->text('description')->nullable()->default(null);
                $table->boolean('is_enabled')->default(1);

                $table->bigInteger('created_by')->unsigned()->default(0);
                $table->bigInteger('updated_by')->unsigned()->nullable()->default(null);
                $table->bigInteger('deleted_by')->unsigned()->nullable()->default(null);
                $table->timestampTz('created_at')->useCurrent();
                $table->timestampTz('updated_at')->nullable()->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
                $table->timestampTz('deleted_at')->nullable()->default(null);

                $table->unique(['merchant_name', 'deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::MERCHANTS);
    }

}
