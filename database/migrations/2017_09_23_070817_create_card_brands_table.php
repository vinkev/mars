<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Mars\Helpers\Constants\CardType;
use Mars\Helpers\Constants\DBTable;

class CreateCardBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable(DBTable::CARD_BRANDS)) {
            Schema::create(DBTable::CARD_BRANDS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('uuid', 32)->index();
                $table->string('brand_name', 255);
                $table->text('description')->nullable()->default(null);
                $table->string('logo', 80);
                $table->boolean('is_enabled')->default(1);

                $table->bigInteger('created_by')->unsigned()->default(0);
                $table->bigInteger('updated_by')->unsigned()->nullable()->default(null);
                $table->bigInteger('deleted_by')->unsigned()->nullable()->default(null);
                $table->timestampTz('created_at')->useCurrent();
                $table->timestampTz('updated_at')->nullable()->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
                $table->timestampTz('deleted_at')->nullable()->default(null);

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::CARD_BRANDS);
    }
}
