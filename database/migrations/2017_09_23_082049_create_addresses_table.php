<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Mars\Helpers\Constants\DBTable;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable(DBTable::ADDRESSES)) {
            Schema::create(DBTable::ADDRESSES, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('addressable_id')->unsigned()->index();
                $table->string('addressable_type')->index();
                $table->string('location_name')->nullable()->default(null);
                $table->string('location_info')->nullable()->default(null);
                $table->string('address');
                $table->string('city')->nullable()->default(null);
                $table->string('zip_code', 5)->nullable()->default(null);
                $table->json('office_phone')->nullable()->default(null);
                $table->json('fax_phone')->nullable()->default(null);

                $table->bigInteger('created_by')->unsigned()->default(0);
                $table->bigInteger('updated_by')->unsigned()->nullable()->default(null);
                $table->timestampTz('created_at')->useCurrent();
                $table->timestampTz('updated_at')->nullable()->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::ADDRESSES);
    }
}
