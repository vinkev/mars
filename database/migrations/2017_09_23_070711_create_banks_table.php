<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Mars\Helpers\Constants\BankType;
use Mars\Helpers\Constants\DBTable;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable(DBTable::BANKS)) {
            Schema::create(DBTable::BANKS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('uuid', 32)->index();
                $table->string('bank_name', 255);
                $table->string('bank_nickname', 255);
                $table->tinyInteger('bank_type')->comment(json_encode(BankType::getConstants(null, true)));
                $table->string('logo', 80);
                $table->string('transfer_bank_code', 4);
                $table->string('website')->nullable()->default(null);
                $table->text('description')->nullable()->default(null);
                $table->boolean('is_enabled')->default(1);

                $table->bigInteger('created_by')->unsigned()->default(0);
                $table->bigInteger('updated_by')->unsigned()->nullable()->default(null);
                $table->bigInteger('deleted_by')->unsigned()->nullable()->default(null);
                $table->timestampTz('created_at')->useCurrent();
                $table->timestampTz('updated_at')->nullable()->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
                $table->timestampTz('deleted_at')->nullable()->default(null);

                $table->unique(['name', 'deleted_at']);
                $table->unique(['transfer_bank_code', 'deleted_at']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::BANKS);
    }
}
