<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Mars\Helpers\Constants\DBTable;
use Mars\Models\Address;
use Ramsey\Uuid\Uuid;

class AlterAddressesTableAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable(DBTable::ADDRESSES)) {
            Schema::table(DBTable::ADDRESSES, function (Blueprint $table) {
                $table->renameColumn('city', 'state');
            });

            Schema::table(DBTable::ADDRESSES, function (Blueprint $table) {
                $table->string('uuid', 32)->index()->after('id');
                $table->boolean('is_enabled')->default(true)->after('fax_phone');
                $table->bigInteger('deleted_by')->unsigned()->nullable()->default(null)->after('updated_by');
                $table->timestampTz('deleted_at')->nullable()->default(null)->after('updated_at');
                $table->string('city', 128)->nullable()->default(null)->after('address');
                $table->string('country', 2)->after('state');
                $table->string('state', 2)->nullable()->default(null)->change();
            });
        }

        $addresses = Address::all();
        DB::transaction(function () use ($addresses) {
            foreach ($addresses as $address) {
                $address->uuid = str_replace('-', '', Uuid::uuid4());
                $address->save();
            }
        }, 2);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable(DBTable::ADDRESSES)) {
            Schema::table(DBTable::ADDRESSES, function (Blueprint $table) {
                $table->dropColumn('city');
            });

            Schema::table(DBTable::ADDRESSES, function (Blueprint $table) {
                $table->renameColumn('state', 'city');
                $table->dropColumn('uuid');
                $table->dropColumn('is_enabled');
                $table->dropColumn('deleted_by');
                $table->dropColumn('deleted_at');
                $table->dropColumn('country');
            });
        }
    }
}
