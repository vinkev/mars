<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Mars\Helpers\Constants\DBTable;

class CreatePromosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable(DBTable::PROMOS)) {
            Schema::create(DBTable::PROMOS, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('uuid', 32)->index();
                $table->bigInteger('merchant_id')->unsigned();
                $table->foreign('merchant_id')->references('id')->on(DBTable::MERCHANTS);

                $table->string('promo_title');
                $table->string('promo_subtitle')->nullable()->default(null);
                $table->text('description')->nullable()->default(null);
                $table->text('term_condition')->nullable()->default(null);
                $table->string('image')->nullable()->default(null);
                $table->timestampTz('publish_at')->nullable()->default(null);
                $table->timestampTz('start_at')->nullable()->default(null);
                $table->timestampTz('end_at')->nullable()->default(null);
                $table->boolean('is_enabled')->default(1);

                $table->bigInteger('created_by')->unsigned()->default(0);
                $table->bigInteger('updated_by')->unsigned()->nullable()->default(null);
                $table->bigInteger('deleted_by')->unsigned()->nullable()->default(null);
                $table->timestampTz('created_at')->useCurrent();
                $table->timestampTz('updated_at')->nullable()->default(DB::raw('NULL ON UPDATE CURRENT_TIMESTAMP'));
                $table->timestampTz('deleted_at')->nullable()->default(null);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DBTable::PROMOS);
    }

}
