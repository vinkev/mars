# ************************************************************
# Sequel Pro SQL dump
# Version 5224
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.19)
# Database: mars
# Generation Time: 2017-09-28 18:07:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table abilities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `abilities`;

CREATE TABLE `abilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_id` int(10) unsigned DEFAULT NULL,
  `entity_type` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `only_owned` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `abilities_unique_index` (`name`,`entity_id`,`entity_type`,`only_owned`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `addressable_id` bigint(20) unsigned NOT NULL,
  `addressable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `office_phone` json DEFAULT NULL,
  `fax_phone` json DEFAULT NULL,
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `updated_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `addresses_uuid_index` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;

INSERT INTO `addresses` (`id`, `uuid`, `addressable_id`, `addressable_type`, `location_name`, `location_info`, `address`, `city`, `zip_code`, `office_phone`, `fax_phone`, `created_by`, `updated_by`, `created_at`, `updated_at`)
VALUES
	(1,'',1,'banks',NULL,NULL,'Jl. Jend. Sudirman Kav. 44-46','Jakarta','10210','[\"(021) 2510244\", \"(021) 2510254\", \"(021) 2510269\"]','[\"(021) 2500065\", \"(021) 2500077\"]',1,1,'2017-09-26 10:30:20','2017-09-29 00:24:48'),
	(2,'',2,'banks','Plaza Mandiri',NULL,'Jl. Gatot Subroto Kav. 36-38','Jakarta','12190','[\"(021) 5245006\", \"(021) 5245858\", \"(021) 5245849\", \"(021) 52997777\"]','[\"(021) 5263459\", \"(021) 5263460\", \"(021) 5268246\", \"(021) 52997735\"]',1,1,'2017-09-27 02:08:51','2017-09-28 17:46:53'),
	(3,'',3,'banks',NULL,NULL,'Jl. Jend. Sudirman Kav. 1','Jakarta','10220','[\"(021) 2511218\", \"(021) 2511219\", \"(021) 2511220\", \"(021) 2511222\"]','[\"(021) 2511221\", \"(021) 5709506\"]',1,NULL,'2017-09-27 03:47:48','2017-09-29 00:24:52'),
	(4,'',4,'banks','Gedung Menara BTN',NULL,'Jl. Gajah Mada No. 1','Jakarta','10130','[\"(021) 2310490\", \"(021) 6336789\", \"(021) 26533555\"]','[\"(021) 6346704\"]',1,NULL,'2017-09-27 03:55:49','2017-09-29 00:47:29'),
	(5,'',5,'banks','Menara Bank Danamon',NULL,'Jl. HR. Rasuna Said Blok C No. 10, Karet Setiabudi, Jakarta Selatan','Jakarta','12940','[\"(021) 8064​5000\"]','[\"(021) 80645033\"]',1,NULL,'2017-09-27 04:06:40','2017-09-29 00:47:38'),
	(6,'',6,'banks','Gedung World Trade Center (WTC) II',NULL,'Jl. Jend. Sudirman Kav. 29-31','Jakarta','12920','[\"(021) 5237788\"]','[\"(021) 5237253\"]',1,NULL,'2017-09-27 04:18:05','2017-09-29 00:47:47'),
	(7,'',7,'banks','Menara BCA, Grand Indonesia',NULL,'Jl. M.H Thamrin No. 1','Jakarta','10310','[\"(021) 2358-8000\"]','[\"(021) 2358-8300\"]',1,NULL,'2017-09-27 04:20:55','2017-09-29 00:48:01'),
	(8,'',8,'banks','Gedung Sentral Senayan 3','Lt. 26','Jl. Asia Afrika No. 8, Gelora Bung Karno - Senayan, Jakarta Pusat','Jakarta','10270','[\"(021) 29228888\"]','[\"(021) 29228914\"]',1,NULL,'2017-09-27 04:28:27','2017-09-29 00:48:19'),
	(9,'',9,'banks','Panin Bank Centre',NULL,'Jl. Jend. Sudirman Kav. 1 (Senayan)','Jakarta','10270','[\"(021) 2700545\"]','[\"(021) 2700340\", \"(021) 2700391\"]',1,NULL,'2017-09-27 04:32:18','2017-09-29 00:48:26'),
	(10,'',10,'banks','Graha Niaga / Niaga Tower',NULL,'Jl. Jend. Sudirman Kav. 58','Jakarta','12190','[\"(021) 2505151\", \"(021) 2505252\", \"(021) 2505353\"]','[\"(021) 2505353\", \"(021) 2505205\"]',1,NULL,'2017-09-27 04:36:29','2017-09-29 00:48:34');

/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table assigned_roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `assigned_roles`;

CREATE TABLE `assigned_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `entity_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  KEY `assigned_roles_entity_id_entity_type_index` (`entity_id`,`entity_type`),
  KEY `assigned_roles_role_id_index` (`role_id`),
  CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `assigned_roles` WRITE;
/*!40000 ALTER TABLE `assigned_roles` DISABLE KEYS */;

INSERT INTO `assigned_roles` (`role_id`, `entity_id`, `entity_type`)
VALUES
	(1,1,'Mars\\Models\\User');

/*!40000 ALTER TABLE `assigned_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table banks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `banks`;

CREATE TABLE `banks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bank_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bank_nickname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bank_type` int(11) NOT NULL COMMENT '{"BANK_UMUM_PERSERO":1,"BANK_UMUM_SWASTA_NASIONAL":2,"BANK_PEMBANGUNAN_DAERAH":3,"BANK_ASING":4}',
  `logo` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transfer_bank_code` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `banks_name_deleted_at_unique` (`bank_name`,`deleted_at`),
  UNIQUE KEY `banks_transfer_bank_code_deleted_at_unique` (`transfer_bank_code`,`deleted_at`),
  KEY `banks_uuid_index` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `banks` WRITE;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;

INSERT INTO `banks` (`id`, `uuid`, `bank_name`, `bank_nickname`, `bank_type`, `logo`, `transfer_bank_code`, `website`, `description`, `is_enabled`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'ec2d1cc504884bebab792b5bd281f9f3','PT. BANK RAKYAT INDONESIA (PERSERO), Tbk','BRI',1,'images/banks/BuIbmSfrhDn9oGcMmNv5oitO0cXwUE2bKbJeAur1.png','002','http://www.bri.co.id',NULL,1,1,1,NULL,'2017-09-26 10:30:20','2017-09-27 03:49:09',NULL),
	(2,'a63c6eb26a6c40c1a6ea38025d700c50','PT. BANK MANDIRI (PERSERO), Tbk','Bank Mandiri',1,'images/banks/iADdsQ7P3Mx3idGui3qSA2m3QAeViMJp1DnsU8By.png','008','http://www.bankmandiri.co.id',NULL,1,1,1,NULL,'2017-09-27 02:08:51','2017-09-27 03:50:25',NULL),
	(3,'b41c0228465e46a1b326610a285227ba','PT. BANK NEGARA INDONESIA (PERSERO), Tbk','BNI',1,'images/banks/md4tbZruyKEFFTWpXUrscqL3JrGFctA19fLeLkvv.png','009','http://www.bni.co.id',NULL,1,1,NULL,NULL,'2017-09-27 03:47:47','2017-09-27 03:47:47',NULL),
	(4,'949ba439f37a477481f1b9f756e2d67e','PT. BANK TABUNGAN NEGARA (PERSERO), Tbk','BTN',1,'images/banks/WAYEYpgN7o70JOt1thIEOOAp5mfG2t7IydykTgNU.png','200','http://www.btn.co.id',NULL,1,1,NULL,NULL,'2017-09-27 03:55:48','2017-09-27 03:55:48',NULL),
	(5,'1c10606b638441548d1a3470dccfb945','PT. BANK DANAMON INDONESIA, Tbk','Bank Danamon',2,'images/banks/G4ZRgLMLsLbo8jQ8irTsHb3F3eytqa8pmjeZNS7x.png','011','https://www.danamon.co.id',NULL,1,1,1,NULL,'2017-09-27 04:06:40','2017-09-27 11:15:03',NULL),
	(6,'2951b2c6858748fba3446523282874ce','PT. BANK PERMATA, Tbk','Permata Bank',2,'images/banks/GFOlm8bTWoYMoCq2kYzO00RWmJOP4snXAcGEMZi4.png','011','https://www.permatabank.com/',NULL,1,1,1,NULL,'2017-09-27 04:18:05','2017-09-27 04:37:07',NULL),
	(7,'c7e92de0407b44ca992a41abf727b285','PT. BANK CENTRAL ASIA, Tbk','BCA',2,'images/banks/6UvYDcUYvKjb7BXxJcPTLmxop2oSFgxMDSiUskqN.png','014','http://www.klikbca.com',NULL,1,1,NULL,NULL,'2017-09-27 04:20:55','2017-09-27 04:20:55',NULL),
	(8,'46f2ca8f6e4845af82e64a0fdf53eaf4','PT. BANK MAYBANK INDONESIA, Tbk​','BII Maybank',2,'images/banks/OSjN2eQ5EcKr4nxH3Ani384J2Hy9bq223dK75Bn6.png','016','https://maybank.co.id',NULL,1,1,NULL,NULL,'2017-09-27 04:28:27','2017-09-27 04:28:27',NULL),
	(9,'a4ed9790f51a4961a60fd07f43fc109c','PT. PAN INDONESIA BANK, Tbk','Bank Panin',2,'images/banks/qBGyZ5zFePn8hpdT5iT6KeZtpjv4CoB77rB981XS.png','019','http://www.panin.co.id',NULL,1,1,1,NULL,'2017-09-27 04:32:18','2017-09-27 11:38:48',NULL),
	(10,'c4784935bcaf4aac940bc763829ba6bc','PT. BANK CIMB NIAGA, Tbk','CIMB Niaga',2,'images/banks/iZjJ2UEcYhOrdT5CA5qyVaSyOS8RLLYEywIEIscD.png','022','https://www.cimbniaga.com/',NULL,1,1,NULL,NULL,'2017-09-27 04:36:29','2017-09-27 04:36:29',NULL);

/*!40000 ALTER TABLE `banks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table card_brands
# ------------------------------------------------------------

DROP TABLE IF EXISTS `card_brands`;

CREATE TABLE `card_brands` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `brand_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `logo` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `card_brands_uuid_index` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `card_brands` WRITE;
/*!40000 ALTER TABLE `card_brands` DISABLE KEYS */;

INSERT INTO `card_brands` (`id`, `uuid`, `brand_name`, `description`, `logo`, `is_enabled`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'4d6d3341ea38408f925b7e8c7d63dbe1','Visa',NULL,'images/brands/N1yPtmh698scVzmWWgcarmbiudnlFUnushpb0hlj.png',1,1,1,NULL,'2017-09-25 07:07:09','2017-09-25 14:11:07',NULL),
	(2,'c291a65bcb754fb6ad43e73d6678d954','Master Card',NULL,'images/brands/qMbBecQqJwHAxdpWsXenBQXLANVaBKA3e6lO6IhB.png',1,1,1,NULL,'2017-09-25 07:10:22','2017-09-25 07:20:44',NULL),
	(3,'118e1fd074c944ada33a6f803fd4f6c9','Maestro',NULL,'images/brands/iSeASZn5YmnztBIkUcnidZQ2dsbrI6hY9TGiWPQa.png',1,1,NULL,NULL,'2017-09-25 07:22:22','2017-09-25 07:22:22',NULL),
	(4,'63bc06bc271845d49555e5c8683e434b','JCB',NULL,'images/brands/CcV6C5x3hTMP6ArtKC4Y15Vcyt4haEOAvO1Ijshn.png',1,1,NULL,NULL,'2017-09-25 07:40:07','2017-09-25 07:40:07',NULL),
	(5,'256b4d64e8ce432d89c658804ccf332e','Cirrus',NULL,'images/brands/0XnNo4XT1eqT3sX4d1lpN7W10HPfWHlvUFMGeOBC.png',1,1,NULL,NULL,'2017-09-25 07:43:33','2017-09-25 07:43:33',NULL),
	(6,'decf6d4a47a64df683156b5974343299','BCA Card',NULL,'images/brands/ovbIN27wjvwTyU9d3OLek143JXL85PdzXFFwEW1F.jpeg',1,1,NULL,NULL,'2017-09-27 16:56:28','2017-09-27 16:56:28',NULL);

/*!40000 ALTER TABLE `card_brands` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table cards
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cards`;

CREATE TABLE `cards` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `bank_id` bigint(20) unsigned NOT NULL,
  `card_brand_id` bigint(20) unsigned NOT NULL,
  `card_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_type` tinyint(4) NOT NULL,
  `image` varchar(80) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cards_bank_id_card_name_deleted_at_unique` (`bank_id`,`card_name`,`deleted_at`),
  KEY `cards_card_brand_id_foreign` (`card_brand_id`),
  KEY `cards_uuid_index` (`uuid`),
  CONSTRAINT `cards_bank_id_foreign` FOREIGN KEY (`bank_id`) REFERENCES `banks` (`id`),
  CONSTRAINT `cards_card_brand_id_foreign` FOREIGN KEY (`card_brand_id`) REFERENCES `card_brands` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `cards` WRITE;
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;

INSERT INTO `cards` (`id`, `uuid`, `bank_id`, `card_brand_id`, `card_name`, `card_type`, `image`, `is_enabled`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,'e4b4e85e973741f9be3a21faaf332e62',7,6,'BCA Everyday Card',1,'images/cards/0QiC12aYXuB3MyIsEDQoM2hLQemNSiAyZdJe8AXe.jpeg',1,1,1,NULL,'2017-09-28 03:41:55','2017-09-28 11:14:51',NULL),
	(2,'2855ae14737c4bfa989e06c4cbb20bc7',7,6,'BCA Card Gold',1,'images/cards/tCtBnkrg2iws1DxefuQeizSCfq5xcxvtjbrccCK9.jpeg',1,1,NULL,NULL,'2017-09-28 04:20:34','2017-09-28 04:20:34',NULL),
	(3,'4e9769da21c64f4ab31cf07741c4ad25',7,6,'BCA Card Platinum',1,'images/cards/YfnNwfbbJzVjBAmsyQZYkyiJth1KmP6DZ5fa5242.jpeg',1,1,NULL,NULL,'2017-09-28 04:26:13','2017-09-28 04:26:13',NULL),
	(4,'98ae4339d3224dc5b5c9b358ad38565b',7,6,'BCA Card Indomaret',1,'images/cards/pbRk95wrrv5nctFDZ8T7jMhpJQcJu0w9HrFDLLu3.jpeg',1,1,NULL,NULL,'2017-09-28 04:27:42','2017-09-28 04:27:42',NULL),
	(5,'76ba8ba495d84f1f9b3d3590818b345c',7,2,'BCA Mastercard Platinum',1,'images/cards/QNyIVPssubgM3YybqE0dwJH3h2AzQkJRNiCD50uz.jpeg',1,1,1,NULL,'2017-09-28 04:31:22','2017-09-28 04:38:37',NULL),
	(6,'edb2932307704f58b4b656b5e2bed0e6',7,1,'BCA Visa Batman',1,'images/cards/VIpXmsXmPttfZvc4H5v625iVAqxnTOxJDt1R1NLE.jpeg',1,1,1,NULL,'2017-09-28 04:36:33','2017-09-28 04:38:16',NULL),
	(7,'4190b0f785854d87b2b0e1990d1dcb66',7,1,'BCA Visa Gold',1,'images/cards/pkiLQKHBvUToKnr42XLiL5R8t35A73yyf3TKA9wo.jpeg',1,1,1,NULL,'2017-09-28 04:37:51','2017-09-28 13:17:19',NULL),
	(8,'6c322eca3f364582a71d71ffe420c7fd',7,2,'BCA Mastercard Matahari',1,'images/cards/5kZmI6P3SYkTapk5C72MlLaFNZZWEg3ePYlidagm.jpeg',1,1,NULL,NULL,'2017-09-28 04:40:20','2017-09-28 04:40:20',NULL),
	(9,'07369da9dece48c596465aade89d3a0c',7,1,'BCA Visa Platinum',2,'images/cards/kGuu4HoVeDkKQQZnlh0XY7nP354KVLBRQwOXbYqS.jpeg',1,1,1,NULL,'2017-09-28 04:41:36','2017-09-28 20:18:57',NULL),
	(10,'d64e430cac2940a48ed403a65f2340fc',7,2,'BCA Mastercard Black',1,'images/cards/CLS8GAevwIpLHlAVTi5ULTRRgQn1coTGdorLgoop.jpeg',1,1,NULL,NULL,'2017-09-28 04:47:30','2017-09-28 04:47:30',NULL),
	(11,'952d10b318b34d1595b68df5b05e5c61',7,1,'BCA Visa Black',1,'images/cards/eY9YEPG8EUTW4ji1zuuB2mL3g5eamrpKMsc6LPq5.jpeg',1,1,1,NULL,'2017-09-28 04:49:26','2017-09-28 13:36:27',NULL),
	(12,'0a43e482f67e4c519e04996e5731681b',7,1,'BCA Singapore Airlines KrisFlyer Visa Signature',1,'images/cards/76FHZbbyEfwgWDwYl0s6DsLf3cVHw4O3NGKsCXd5.jpeg',1,1,NULL,NULL,'2017-09-28 04:50:38','2017-09-28 04:50:38',NULL),
	(13,'2473e48bf05d4ac9901f1d50517f633c',7,1,'BCA Singapore Airlines PPS Club Visa Infinite',1,'images/cards/kq3wzKZ7dLMDb6h79aoJLuePJqkdQUgBQzLMDfXt.jpeg',1,1,NULL,NULL,'2017-09-28 04:51:42','2017-09-28 04:51:42',NULL);

/*!40000 ALTER TABLE `cards` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table merchants
# ------------------------------------------------------------

DROP TABLE IF EXISTS `merchants`;

CREATE TABLE `merchants` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merchant_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `merchants_merchant_name_deleted_at_unique` (`merchant_name`,`deleted_at`),
  KEY `merchants_uuid_index` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(17,'2014_10_12_000000_create_users_table',1),
	(18,'2014_10_12_100000_create_password_resets_table',1),
	(19,'2017_09_18_172958_create_bouncer_tables',1),
	(20,'2017_09_19_035850_create_user_profiles_table',1),
	(47,'2017_09_23_070711_create_banks_table',2),
	(48,'2017_09_23_070817_create_card_brands_table',2),
	(49,'2017_09_23_070918_create_cards_table',2),
	(51,'2017_09_23_082049_create_addresses_table',3),
	(52,'2017_09_28_045319_create_merchants_table',4),
	(53,'2017_09_28_049149_create_promos_table',4);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table permissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `ability_id` int(10) unsigned NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `entity_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forbidden` tinyint(1) NOT NULL DEFAULT '0',
  KEY `permissions_entity_id_entity_type_index` (`entity_id`,`entity_type`),
  KEY `permissions_ability_id_index` (`ability_id`),
  CONSTRAINT `permissions_ability_id_foreign` FOREIGN KEY (`ability_id`) REFERENCES `abilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table promos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `promos`;

CREATE TABLE `promos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_method_id` bigint(20) unsigned NOT NULL,
  `payment_method_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merchant_id` bigint(20) unsigned NOT NULL,
  `promo_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promo_subtitle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `term_condition` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publish_at` timestamp NULL DEFAULT NULL,
  `start_at` timestamp NULL DEFAULT NULL,
  `end_at` timestamp NULL DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `updated_by` bigint(20) DEFAULT NULL,
  `deleted_by` bigint(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `promos_merchant_id_foreign` (`merchant_id`),
  KEY `promos_uuid_index` (`uuid`),
  CONSTRAINT `promos_merchant_id_foreign` FOREIGN KEY (`merchant_id`) REFERENCES `merchants` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`id`, `name`, `title`, `level`, `created_at`, `updated_at`)
VALUES
	(1,'super-admin','Super Admin',NULL,'2017-09-22 06:19:33','2017-09-23 02:37:42'),
	(2,'admin','Admin',NULL,'2017-09-22 18:10:02','2017-09-22 18:10:02'),
	(3,'user','User',NULL,'2017-09-23 02:30:05','2017-09-23 02:30:05');

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_profiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_profiles`;

CREATE TABLE `user_profiles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `name` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` tinyint(4) NOT NULL COMMENT '{"MALE":1,"FEMALE":2,"OTHER":3}',
  `cell_phone` json DEFAULT NULL,
  `photo` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_profiles_user_id_foreign` (`user_id`),
  CONSTRAINT `user_profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `user_profiles` WRITE;
/*!40000 ALTER TABLE `user_profiles` DISABLE KEYS */;

INSERT INTO `user_profiles` (`id`, `user_id`, `name`, `gender`, `cell_phone`, `photo`, `created_at`, `updated_at`)
VALUES
	(1,1,'Kevin Linggajaya',1,'{\"verified\": false, \"phone_number\": \"+6281808078020\"}',NULL,'2017-09-22 06:19:33','2017-09-22 06:19:33');

/*!40000 ALTER TABLE `user_profiles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '{"PENDING":1,"ACTIVE":2,"INACTIVE":3,"BANNED":4}',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_uuid_index` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `uuid`, `email`, `password`, `user_status`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'e80aa7456ed340b48f1210c21593efd1','kevin.linggajaya@gmail.com','$2y$10$pU3utI6MfgGo39fbbQS5NermxAUk.iZ4QdLuqqKOO1hKfm6gltdSu',1,'n6ZaSsyNyNF8N6h78pf0a9dguFLg4RPXBI0MnBz9wq3jQTe3mTkMWumtVmhW','2017-09-22 06:19:33','2017-09-22 13:35:41');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
