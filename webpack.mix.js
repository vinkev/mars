var mix = require('laravel-mix');

var CleanWebpackPlugin = require('clean-webpack-plugin');

// paths to clean
var pathsToClean = [
    'public/assets/app/js',
    'public/assets/app/css',
    'public/assets/admin/js',
    'public/assets/admin/css',
    'public/assets/auth/css',
];

// the clean options to use
var cleanOptions = {};

mix.webpackConfig({
    plugins: [
        new CleanWebpackPlugin(pathsToClean, cleanOptions)
    ]
});

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
|--------------------------------------------------------------------------
| Core
|--------------------------------------------------------------------------
|
*/

mix.scripts([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/pace-progress/pace.js',

], 'public/assets/app/js/app.js').version();

mix.styles([
    'node_modules/bootstrap/dist/css/bootstrap.css',
    'node_modules/font-awesome/css/font-awesome.css',
    'node_modules/pace-progress/themes/blue/pace-theme-minimal.css',
], 'public/assets/app/css/app.css').version();

mix.copy([
    'node_modules/gentelella/vendors/bootstrap/dist/fonts',
    'node_modules/font-awesome/fonts'
], 'public/assets/app/fonts');


/*
 |--------------------------------------------------------------------------
 | Auth
 |--------------------------------------------------------------------------
 |
 */

mix.styles('resources/assets/auth/css/login.css', 'public/assets/auth/css/login.css').version();
// mix.styles('resources/assets/auth/css/register.css', 'public/assets/auth/css/register.css').version();
// mix.styles('resources/assets/auth/css/passwords.css', 'public/assets/auth/css/passwords.css').version();

mix.styles([
    'node_modules/gentelella/vendors/animate.css/animate.css',
    'node_modules/gentelella/build/css/custom.css',
], 'public/assets/auth/css/auth.css').version();



/*
 |--------------------------------------------------------------------------
 | Admin
 |--------------------------------------------------------------------------
 |
 */

mix.scripts([
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.js',
    'node_modules/pnotify/dist/pnotify.js',
    'node_modules/pnotify/dist/pnotify.animate.js',
    'node_modules/pnotify/dist/pnotify.buttons.js',
    'node_modules/pnotify/dist/pnotify.mobile.js',
    'node_modules/pnotify/dist/pnotify.nonblock.js',
    'node_modules/gentelella/src/js/helpers/smartresize.js',
    'node_modules/gentelella/src/js/custom.js'
], 'public/assets/admin/js/admin.js').version();

mix.styles([
    'node_modules/gentelella/vendors/animate.css/animate.css',
    'node_modules/pnotify/dist/pnotify.css',
    'node_modules/pnotify/dist/pnotify.buttons.css',
    'node_modules/pnotify/dist/pnotify.mobile.css',
    'node_modules/pnotify/dist/pnotify.nonblock.css',
    'node_modules/pnotify/dist/pnotify.brighttheme.css',
    'resources/assets/vendor/switchery/switchery.css',
    'node_modules/gentelella/build/css/custom.css',
    'resources/assets/admin/css/admin.css'
], 'public/assets/admin/css/admin.css').version();


mix.scripts([
    'node_modules/select2/dist/js/select2.full.js',
    'node_modules/gentelella/vendors/moment/moment.js',
    'node_modules/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js',
    'resources/assets/vendor/switchery/switchery.js',
    'resources/assets/vendor/bootstrap-fileinput/js/plugins/purify.js',
    'resources/assets/vendor/bootstrap-fileinput/js/fileinput.js',
    'resources/assets/vendor/bootstrap-fileinput/themes/fa/theme.js',
    'node_modules/jquery.hotkeys/jquery.hotkeys.js',
    'node_modules/summernote/dist/summernote.js',
    'resources/assets/admin/js/edit.js'
], 'public/assets/admin/js/edit.js').version();

mix.styles([
    'node_modules/select2/dist/css/select2.css',
    'node_modules/select2-bootstrap-theme/dist/select2-bootstrap.css',
    'node_modules/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css',
    'resources/assets/vendor/switchery/switchery.css',
    'resources/assets/vendor/bootstrap-fileinput/css/fileinput.css',
    'node_modules/summernote/dist/summernote.css',
    'resources/assets/admin/css/edit.css'
], 'public/assets/admin/css/edit.css').version();

mix.scripts([
    'node_modules/gentelella/vendors/Flot/jquery.flot.js',
    'node_modules/gentelella/vendors/Flot/jquery.flot.time.js',
    'node_modules/gentelella/vendors/Flot/jquery.flot.pie.js',
    'node_modules/gentelella/vendors/Flot/jquery.flot.stack.js',
    'node_modules/gentelella/vendors/Flot/jquery.flot.resize.js',

    'node_modules/gentelella/production/js/flot/jquery.flot.orderBars.js',
    'node_modules/gentelella/production/js/flot/date.js',
    'node_modules/gentelella/production/js/flot/curvedLines.js',
    'node_modules/gentelella/production/js/flot/jquery.flot.spline.js',

    'node_modules/gentelella/production/js/moment/moment.min.js',
    'node_modules/gentelella/production/js/datepicker/daterangepicker.js',


    'node_modules/gentelella/vendors/Chart.js/dist/Chart.js',

    // 'resources/assets/admin/js/dashboard.js',
], 'public/assets/admin/js/dashboard.js').version();

// mix.styles([
//     'resources/assets/admin/css/dashboard.css',
// ], 'public/assets/admin/css/dashboard.css').version();

mix.copy([
    'node_modules/summernote/dist/font',
], 'public/assets/admin/css/font');

mix.browserSync({
    files: [
        // 'app/*.*',
        // 'app/**/*.*',
        'public/assets/**/*.css',
        'public/assets/**/*.js',
        'resources/views/**/*.blade.php',
    ],
    open: false,
    proxy: 'mars.localhost',
    notify: true
});