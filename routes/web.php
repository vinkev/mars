<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::group(['namespace' => 'Auth'], function () {
    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset');

});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth.group', 'as' => 'admin.'], function () {
    Route::get('/', 'DashboardController@index')->name('index');
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');

    Route::resource('/users', 'UserController', ['except' => ['show', 'destroy']]);
    Route::resource('/roles', 'RoleController', ['except' => ['show']]);
    Route::resource('/card-brands', 'CardBrandController', ['except' => ['show']]);
    Route::resource('/banks', 'BankController', ['except' => ['show']]);
    Route::resource('/cards', 'CardController', ['except' => ['show']]);

    Route::group(['prefix' => 'merchants/{merchant}', 'as' => 'merchants.'], function () {
        Route::group(['prefix' => 'addresses', 'as' => 'addresses.'], function () {
            Route::post('/', 'MerchantController@storeAddress')->name('store');
            Route::get('/create', 'MerchantController@createAddress')->name('create');
            Route::get('/{address}', 'MerchantController@showAddress')->name('show');
            Route::put('/{address}', 'MerchantController@updateAddress')->name('update');
            Route::delete('/{address}', 'MerchantController@destroyAddress')->name('destroy');
            Route::get('/{address}/edit', 'MerchantController@editAddress')->name('edit');
        });
    });

    Route::resource('/merchants', 'MerchantController', ['except', ['show']]);
    Route::resource('/promos', 'PromoController', ['except', ['show']]);
});
