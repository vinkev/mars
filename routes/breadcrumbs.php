<?php

// User
Breadcrumbs::register('admin.users.index', function ($breadcrumbs) {
});

Breadcrumbs::register('admin.users.create', function ($breadcrumbs) {
    $breadcrumbs->push('Users List', action('Admin\UserController@index'));
    $breadcrumbs->push('Create User', action('Admin\UserController@create'));
});


// Roles
Breadcrumbs::register('admin.roles.create', function ($breadcrumbs) {
    $breadcrumbs->push('Roles List', action('Admin\RoleController@index'));
    $breadcrumbs->push('Create Role', action('Admin\RoleController@create'));
});

Breadcrumbs::register('admin.roles.edit', function ($breadcrumbs, $role_id) {
    $breadcrumbs->push('Roles List', action('Admin\RoleController@index'));
    $breadcrumbs->push('Edit Role', action('Admin\RoleController@edit', $role_id));
});


// Card Brand
Breadcrumbs::register('admin.card-brands.create', function ($breadcrumbs) {
    $breadcrumbs->push('Card Brands List', action('Admin\CardBrandController@index'));
    $breadcrumbs->push('Create Card Brand', action('Admin\CardBrandController@create'));
});

Breadcrumbs::register('admin.card-brands.edit', function ($breadcrumbs, $card_brand_uuid) {
    $breadcrumbs->push('Card Brands List', action('Admin\CardBrandController@index'));
    $breadcrumbs->push('Edit Card Brand', action('Admin\CardBrandController@edit', $card_brand_uuid));
});


// Bank
Breadcrumbs::register('admin.banks.create', function ($breadcrumbs) {
    $breadcrumbs->push('Banks List', action('Admin\BankController@index'));
    $breadcrumbs->push('Create Bank', action('Admin\BankController@create'));
});

Breadcrumbs::register('admin.banks.edit', function ($breadcrumbs, $bank_uuid) {
    $breadcrumbs->push('Banks List', action('Admin\BankController@index'));
    $breadcrumbs->push('Edit Bank', action('Admin\BankController@edit', $bank_uuid));
});


// Card
Breadcrumbs::register('admin.cards.create', function ($breadcrumbs) {
    $breadcrumbs->push('Cards List', action('Admin\CardController@index'));
    $breadcrumbs->push('Create Card', action('Admin\CardController@create'));
});

Breadcrumbs::register('admin.cards.edit', function ($breadcrumbs, $card_uuid) {
    $breadcrumbs->push('Cards List', action('Admin\CardController@index'));
    $breadcrumbs->push('Edit Card', action('Admin\CardController@edit', $card_uuid));
});


// Merchant
Breadcrumbs::register('admin.merchants.create', function ($breadcrumbs) {
    $breadcrumbs->push('Merchants List', action('Admin\MerchantController@index'));
    $breadcrumbs->push('Create Merchant', action('Admin\MerchantController@create'));
});

Breadcrumbs::register('admin.merchants.edit', function ($breadcrumbs, $merchant_uuid) {
    $merchant = app('Mars\Repositories\Interfaces\MerchantRepository')->getMerchantByUuid($merchant_uuid, false)->first();

    $breadcrumbs->push('Merchants List', action('Admin\MerchantController@index'));
    $breadcrumbs->push('Edit Merchant ['.$merchant->merchant_name.']', action('Admin\MerchantController@edit', $merchant_uuid));
});

Breadcrumbs::register('admin.merchants.addresses.create', function($breadcrumbs, $merchant_uuid){
    $breadcrumbs->parent('admin.merchants.edit', $merchant_uuid);
    $breadcrumbs->push('Create Address', action('Admin\MerchantController@createAddress', $merchant_uuid));
});

Breadcrumbs::register('admin.merchants.addresses.edit', function($breadcrumbs, $merchant_uuid, $address_uuid){
    $breadcrumbs->parent('admin.merchants.edit', $merchant_uuid);
    $breadcrumbs->push('Edit Address', action('Admin\MerchantController@createAddress', [$merchant_uuid, $address_uuid]));
});


// Promo
Breadcrumbs::register('admin.promos.create', function ($breadcrumbs) {
    $breadcrumbs->push('Promos List', action('Admin\PromoController@index'));
    $breadcrumbs->push('Create Merchant', action('Admin\PromoController@create'));
});

Breadcrumbs::register('admin.promos.edit', function ($breadcrumbs, $promo_uuid) {
    $breadcrumbs->push('Promos List', action('Admin\PromoController@index'));
    $breadcrumbs->push('Edit Merchant', action('Admin\PromoController@edit', $promo_uuid));
});
